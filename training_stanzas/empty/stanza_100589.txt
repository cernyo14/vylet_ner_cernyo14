1	Jak	Db--------------	jak	O
2	jitřenka	NNFS1-----A-----	jitřenka	O
3	když	J,--------------	když	O
4	na	RR--6-----------	na	O
5	úsvitě	NNIS6-----A-----	úsvit	O
6	vzplane	VB-S---3P-AA---P	vzplanout	O
7	tak	Db--------------	tak	O
8	zjev	NNIS1-----A-----	zjev	O
9	Váš	PSIS1-P2--------	váš	O
10	zářivý	AAIS1----1A-----	zářivý	O
11	nám	PP-P3--1--------	my	O
12	do	RR--2-----------	do	O
13	tmy	NNFS2-----A-----	tma	O
14	vzplál	VpIS---3R-AA---P	vzplanout	O
15	tak	Db--------------	tak	O
16	ze	RV--2-----------	z	O
17	země	NNFS2-----A-----	země	O
18	té	PDFS2-----------	ten	O
19	bídně	Dg-------1A-----	bídně	O
20	udupané	AAFS2----1A-----	udupaný	O
21	vzrost	VpIS---3R-AA--6P	vzrůst	O
22	českým	AAFP3----1A-----	český	O
23	ženám	NNFP3-----A-----	žena	O
24	světlý	AAIS1----1A-----	světlý	O
25	ideál	NNIS1-----A-----	ideál	O
26	Jak	Db--------------	jak	O
27	zářný	AAIS1----1A-----	zářný	O
28	maják	NNIS1-----A-----	maják	O
29	před	RR--7-----------	před	O
30	úskalím	NNNS7-----A-----	úskalí	O
31	chrání	VB-S---3P-AA---I	chránit	O
32	tak	Db--------------	tak	O
33	čisté	AAFS2----1A-----	čistý	O
34	Vaší	PSFS2-P2--------	váš	O
35	lásky	NNFS2-----A-----	láska	O
36	horování	NNNS2-----A-----	horování	O
37	výš	Dg-------2A-----	vysoko	O
38	neslo	VpNS---3R-AA---I	nést	O
39	je	PPFP4--3--------	oni	O
40	ze	RV--2-----------	z	O
41	lhostejnosti	NNFS2-----A-----	lhostejnost	O
42	vln	NNFP2-----A-----	vlna	O
43	a	J^--------------	a	O
44	učilo	VpNS---3R-AA---I	učit	O
45	je	PPFP4--3--------	oni	O
46	ve	RV--6-----------	v	O
47	všem	PLNS6-----------	všechen	O
48	vždy	Db--------A-----	vždy	O
49	a	J^--------------	a	O
50	všude	Db--------------	všude	O
51	se	P7-X4-----------	se	O
52	v	RR--6-----------	v	O
53	lásce	NNFS6-----A-----	láska	O
54	znáti	Vf--------A---2I	znát	O
55	k	RR--3-----------	k	O
56	rodné	AAFS3----1A-----	rodný	O
57	zemi	NNFS3-----A-----	země	O
58	chudé	AAMP4----1A-----	chudý	O
59	byť	J,--------------	byť	O
60	její	PSIS1FS3--------	její	O
61	los	NNIS1-----A-----	los	O
62	byl	VpIS---3R-AA---I	být	O
63	hořkých	AAIP2----1A-----	hořký	O
64	trudů	NNIP2-----A-----	trud	O
65	pln	ACIS------A-----	plný	O

