Using existent pickle file: pkl/Stanzas_joined_real_persons_word_2_vec_lemma.txt.pkl
--- Stanzas_joined_real_persons ---
238561 train sentences
42098 dev sentences
49527 test sentences
Pad words to uniform length for characters embeddings
Words padded to 25 characters
LSTM-Size: [100, 100]
_____________________________________________________________________________________________________________________________
Layer (type)                             Output Shape               Param #        Connected to                              
=============================================================================================================================
char_input (InputLayer)                  (None, None, 25)           0                                                        
_____________________________________________________________________________________________________________________________
char_emd (TimeDistributed)               (None, None, 25, 25)       2375           char_input[0][0]                          
_____________________________________________________________________________________________________________________________
words_input (InputLayer)                 (None, None)               0                                                        
_____________________________________________________________________________________________________________________________
morph_input (InputLayer)                 (None, None)               0                                                        
_____________________________________________________________________________________________________________________________
real_tokens_input (InputLayer)           (None, None)               0                                                        
_____________________________________________________________________________________________________________________________
char_cnn (TimeDistributed)               (None, None, 25, 30)       2280           char_emd[0][0]                            
_____________________________________________________________________________________________________________________________
word_embeddings (Embedding)              (None, None, 100)          18622400       words_input[0][0]                         
_____________________________________________________________________________________________________________________________
morph_emebddings (Embedding)             (None, None, 10)           36420          morph_input[0][0]                         
_____________________________________________________________________________________________________________________________
real_tokens_emebddings (Embedding)       (None, None, 10)           3498160        real_tokens_input[0][0]                   
_____________________________________________________________________________________________________________________________
char_pooling (TimeDistributed)           (None, None, 30)           0              char_cnn[0][0]                            
_____________________________________________________________________________________________________________________________
concatenate_1 (Concatenate)              (None, None, 150)          0              word_embeddings[0][0]                     
                                                                                   morph_emebddings[0][0]                    
                                                                                   real_tokens_emebddings[0][0]              
                                                                                   char_pooling[0][0]                        
_____________________________________________________________________________________________________________________________
shared_varLSTM_1 (Bidirectional)         (None, None, 200)          200800         concatenate_1[0][0]                       
_____________________________________________________________________________________________________________________________
shared_varLSTM_2 (Bidirectional)         (None, None, 200)          240800         shared_varLSTM_1[0][0]                    
_____________________________________________________________________________________________________________________________
Stanzas_joined_real_persons_hidden_lin_l (None, None, 9)            1809           shared_varLSTM_2[0][0]                    
_____________________________________________________________________________________________________________________________
Stanzas_joined_real_persons_crf (ChainCR (None, None, 9)            99             Stanzas_joined_real_persons_hidden_lin_lay
=============================================================================================================================
Total params: 22,605,143
Trainable params: 3,982,743
Non-trainable params: 18,622,400
_____________________________________________________________________________________________________________________________

--------- Epoch 1 -----------
1634.89 sec for training (1634.89 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.939, Rec: 0.936, F1: 0.9374
Test-Data: Prec: 0.947, Rec: 0.940, F1: 0.9433

Scores from epoch with best dev-scores:
  Dev-Score: 0.9374
  Test-Score 0.9433

272.97 sec for evaluation

--------- Epoch 2 -----------
1828.63 sec for training (3463.52 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.938, Rec: 0.946, F1: 0.9423
Wrong BIO-Encoding 1/13832 labels, 0.01%
Wrong BIO-Encoding 1/13832 labels, 0.01%
Test-Data: Prec: 0.940, Rec: 0.948, F1: 0.9439

Scores from epoch with best dev-scores:
  Dev-Score: 0.9423
  Test-Score 0.9439

286.92 sec for evaluation

--------- Epoch 3 -----------
1869.58 sec for training (5333.10 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.932, Rec: 0.928, F1: 0.9301
Test-Data: Prec: 0.934, Rec: 0.929, F1: 0.9315

Scores from epoch with best dev-scores:
  Dev-Score: 0.9423
  Test-Score 0.9439

275.94 sec for evaluation

--------- Epoch 4 -----------
1925.69 sec for training (7258.79 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.910, Rec: 0.922, F1: 0.9155
Wrong BIO-Encoding 1/13905 labels, 0.01%
Wrong BIO-Encoding 1/13905 labels, 0.01%
Test-Data: Prec: 0.909, Rec: 0.921, F1: 0.9148

Scores from epoch with best dev-scores:
  Dev-Score: 0.9423
  Test-Score 0.9439

296.02 sec for evaluation

--------- Epoch 5 -----------
1983.50 sec for training (9242.29 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.916, Rec: 0.922, F1: 0.9189
Test-Data: Prec: 0.917, Rec: 0.923, F1: 0.9200

Scores from epoch with best dev-scores:
  Dev-Score: 0.9423
  Test-Score 0.9439

297.36 sec for evaluation

--------- Epoch 6 -----------
2072.26 sec for training (11314.55 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.922, Rec: 0.927, F1: 0.9242
Test-Data: Prec: 0.923, Rec: 0.927, F1: 0.9251

Scores from epoch with best dev-scores:
  Dev-Score: 0.9423
  Test-Score 0.9439

295.17 sec for evaluation

--------- Epoch 7 -----------
2132.73 sec for training (13447.27 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.922, Rec: 0.922, F1: 0.9215
Test-Data: Prec: 0.922, Rec: 0.919, F1: 0.9204

Scores from epoch with best dev-scores:
  Dev-Score: 0.9423
  Test-Score 0.9439

306.03 sec for evaluation

--------- Epoch 8 -----------
2254.20 sec for training (15701.47 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.931, Rec: 0.943, F1: 0.9366
Test-Data: Prec: 0.928, Rec: 0.939, F1: 0.9334

Scores from epoch with best dev-scores:
  Dev-Score: 0.9423
  Test-Score 0.9439

292.69 sec for evaluation

--------- Epoch 9 -----------
2355.39 sec for training (18056.86 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.923, Rec: 0.926, F1: 0.9246
Test-Data: Prec: 0.923, Rec: 0.927, F1: 0.9247

Scores from epoch with best dev-scores:
  Dev-Score: 0.9423
  Test-Score 0.9439

326.03 sec for evaluation

--------- Epoch 10 -----------
2282.19 sec for training (20339.04 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.916, Rec: 0.925, F1: 0.9206
Test-Data: Prec: 0.916, Rec: 0.924, F1: 0.9202

Scores from epoch with best dev-scores:
  Dev-Score: 0.9423
  Test-Score 0.9439

338.42 sec for evaluation

--------- Epoch 11 -----------
2339.43 sec for training (22678.47 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.933, Rec: 0.936, F1: 0.9345
Test-Data: Prec: 0.933, Rec: 0.935, F1: 0.9339

Scores from epoch with best dev-scores:
  Dev-Score: 0.9423
  Test-Score 0.9439

333.33 sec for evaluation

--------- Epoch 12 -----------
2373.56 sec for training (25052.03 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.926, Rec: 0.932, F1: 0.9289
Test-Data: Prec: 0.928, Rec: 0.933, F1: 0.9305

Scores from epoch with best dev-scores:
  Dev-Score: 0.9423
  Test-Score 0.9439

323.92 sec for evaluation

--------- Epoch 13 -----------
2419.03 sec for training (27471.07 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.940, Rec: 0.937, F1: 0.9386
Test-Data: Prec: 0.942, Rec: 0.937, F1: 0.9397

Scores from epoch with best dev-scores:
  Dev-Score: 0.9423
  Test-Score 0.9439

336.71 sec for evaluation

--------- Epoch 14 -----------
2442.04 sec for training (29913.11 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 1/11574 labels, 0.01%
Wrong BIO-Encoding 1/11574 labels, 0.01%
Dev-Data: Prec: 0.920, Rec: 0.932, F1: 0.9258
Test-Data: Prec: 0.918, Rec: 0.931, F1: 0.9242

Scores from epoch with best dev-scores:
  Dev-Score: 0.9423
  Test-Score 0.9439

343.27 sec for evaluation

--------- Epoch 15 -----------
2537.54 sec for training (32450.65 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.939, Rec: 0.945, F1: 0.9420
Test-Data: Prec: 0.942, Rec: 0.945, F1: 0.9435

Scores from epoch with best dev-scores:
  Dev-Score: 0.9423
  Test-Score 0.9439

342.65 sec for evaluation

--------- Epoch 16 -----------
2613.53 sec for training (35064.18 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.930, Rec: 0.930, F1: 0.9300
Test-Data: Prec: 0.931, Rec: 0.931, F1: 0.9310

Scores from epoch with best dev-scores:
  Dev-Score: 0.9423
  Test-Score 0.9439

413.53 sec for evaluation

--------- Epoch 17 -----------
2653.18 sec for training (37717.37 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 1/11450 labels, 0.01%
Wrong BIO-Encoding 1/11450 labels, 0.01%
Dev-Data: Prec: 0.933, Rec: 0.935, F1: 0.9340
Test-Data: Prec: 0.932, Rec: 0.933, F1: 0.9322

Scores from epoch with best dev-scores:
  Dev-Score: 0.9423
  Test-Score 0.9439

381.96 sec for evaluation

--------- Epoch 18 -----------
2566.71 sec for training (40284.08 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 1/11564 labels, 0.01%
Wrong BIO-Encoding 1/11564 labels, 0.01%
Dev-Data: Prec: 0.927, Rec: 0.939, F1: 0.9331
Test-Data: Prec: 0.930, Rec: 0.940, F1: 0.9351

Scores from epoch with best dev-scores:
  Dev-Score: 0.9423
  Test-Score 0.9439

364.71 sec for evaluation

--------- Epoch 19 -----------
2579.74 sec for training (42863.82 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 2/11665 labels, 0.02%
Wrong BIO-Encoding 2/11665 labels, 0.02%
Dev-Data: Prec: 0.915, Rec: 0.935, F1: 0.9251
Wrong BIO-Encoding 4/14025 labels, 0.03%
Wrong BIO-Encoding 4/14025 labels, 0.03%
Test-Data: Prec: 0.915, Rec: 0.935, F1: 0.9246

Scores from epoch with best dev-scores:
  Dev-Score: 0.9423
  Test-Score 0.9439

357.47 sec for evaluation

--------- Epoch 20 -----------
2581.50 sec for training (45445.32 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 5/11427 labels, 0.04%
Wrong BIO-Encoding 5/11427 labels, 0.04%
Dev-Data: Prec: 0.937, Rec: 0.937, F1: 0.9373
Test-Data: Prec: 0.939, Rec: 0.938, F1: 0.9386

Scores from epoch with best dev-scores:
  Dev-Score: 0.9423
  Test-Score 0.9439

361.97 sec for evaluation

--------- Epoch 21 -----------
2589.97 sec for training (48035.29 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 1/11494 labels, 0.01%
Wrong BIO-Encoding 1/11494 labels, 0.01%
Dev-Data: Prec: 0.940, Rec: 0.946, F1: 0.9431
Test-Data: Prec: 0.941, Rec: 0.947, F1: 0.9442

Scores from epoch with best dev-scores:
  Dev-Score: 0.9431
  Test-Score 0.9442

360.38 sec for evaluation

--------- Epoch 22 -----------
2641.58 sec for training (50676.87 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 2/11485 labels, 0.02%
Wrong BIO-Encoding 2/11485 labels, 0.02%
Dev-Data: Prec: 0.941, Rec: 0.946, F1: 0.9436
Test-Data: Prec: 0.940, Rec: 0.944, F1: 0.9416

Scores from epoch with best dev-scores:
  Dev-Score: 0.9436
  Test-Score 0.9416

372.55 sec for evaluation

--------- Epoch 23 -----------
2690.94 sec for training (53367.81 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 1/11395 labels, 0.01%
Wrong BIO-Encoding 1/11395 labels, 0.01%
Dev-Data: Prec: 0.944, Rec: 0.942, F1: 0.9432
Wrong BIO-Encoding 2/13661 labels, 0.01%
Wrong BIO-Encoding 2/13661 labels, 0.01%
Test-Data: Prec: 0.947, Rec: 0.943, F1: 0.9453

Scores from epoch with best dev-scores:
  Dev-Score: 0.9436
  Test-Score 0.9416

386.10 sec for evaluation

--------- Epoch 24 -----------
2692.00 sec for training (56059.81 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 2/11558 labels, 0.02%
Wrong BIO-Encoding 2/11558 labels, 0.02%
Dev-Data: Prec: 0.932, Rec: 0.943, F1: 0.9376
Test-Data: Prec: 0.929, Rec: 0.941, F1: 0.9349

Scores from epoch with best dev-scores:
  Dev-Score: 0.9436
  Test-Score 0.9416

387.69 sec for evaluation

--------- Epoch 25 -----------
2734.31 sec for training (58794.12 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 1/11445 labels, 0.01%
Wrong BIO-Encoding 1/11445 labels, 0.01%
Dev-Data: Prec: 0.937, Rec: 0.939, F1: 0.9380
Wrong BIO-Encoding 1/13743 labels, 0.01%
Wrong BIO-Encoding 1/13743 labels, 0.01%
Test-Data: Prec: 0.937, Rec: 0.938, F1: 0.9375

Scores from epoch with best dev-scores:
  Dev-Score: 0.9436
  Test-Score 0.9416

392.62 sec for evaluation
