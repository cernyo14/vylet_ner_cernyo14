Generate new embeddings files for a dataset
Read file: word_2_vec_lemma.txt.gz
Added words: 64
:: Transform Poems_v2 dataset ::
:: Create Train Matrix ::
Unknown-Tokens: 0.05%
:: Create Dev Matrix ::
Unknown-Tokens: 0.04%
:: Create Test Matrix ::
Unknown-Tokens: 0.05%
DONE - Embeddings file saved: pkl/Poems_v2_word_2_vec_lemma.txt.pkl
--- Poems_v2 ---
141589 train sentences
25359 dev sentences
28860 test sentences
Pad words to uniform length for characters embeddings
Words padded to 50 characters
LSTM-Size: [100, 100]
_____________________________________________________________________________________________________________________________
Layer (type)                             Output Shape               Param #        Connected to                              
=============================================================================================================================
char_input (InputLayer)                  (None, None, 50)           0                                                        
_____________________________________________________________________________________________________________________________
char_emd (TimeDistributed)               (None, None, 50, 30)       2850           char_input[0][0]                          
_____________________________________________________________________________________________________________________________
words_input (InputLayer)                 (None, None)               0                                                        
_____________________________________________________________________________________________________________________________
casing_input (InputLayer)                (None, None)               0                                                        
_____________________________________________________________________________________________________________________________
char_cnn (TimeDistributed)               (None, None, 50, 30)       2730           char_emd[0][0]                            
_____________________________________________________________________________________________________________________________
word_embeddings (Embedding)              (None, None, 100)          18619500       words_input[0][0]                         
_____________________________________________________________________________________________________________________________
casing_emebddings (Embedding)            (None, None, 10)           80             casing_input[0][0]                        
_____________________________________________________________________________________________________________________________
char_pooling (TimeDistributed)           (None, None, 30)           0              char_cnn[0][0]                            
_____________________________________________________________________________________________________________________________
concatenate_1 (Concatenate)              (None, None, 140)          0              word_embeddings[0][0]                     
                                                                                   casing_emebddings[0][0]                   
                                                                                   char_pooling[0][0]                        
_____________________________________________________________________________________________________________________________
shared_varLSTM_1 (Bidirectional)         (None, None, 200)          192800         concatenate_1[0][0]                       
_____________________________________________________________________________________________________________________________
shared_varLSTM_2 (Bidirectional)         (None, None, 200)          240800         shared_varLSTM_1[0][0]                    
_____________________________________________________________________________________________________________________________
Poems_v2_hidden_lin_layer (TimeDistribut (None, None, 11)           2211           shared_varLSTM_2[0][0]                    
_____________________________________________________________________________________________________________________________
Poems_v2_crf (ChainCRF)                  (None, None, 11)           143            Poems_v2_hidden_lin_layer[0][0]           
=============================================================================================================================
Total params: 19,061,114
Trainable params: 441,614
Non-trainable params: 18,619,500
_____________________________________________________________________________________________________________________________

--------- Epoch 1 -----------
720.13 sec for training (720.13 total)
-- Poems_v2 --
Wrong BIO-Encoding 1/3969 labels, 0.03%
Wrong BIO-Encoding 1/3969 labels, 0.03%
Dev-Data: Prec: 0.789, Rec: 0.796, F1: 0.7925
Wrong BIO-Encoding 2/4505 labels, 0.04%
Wrong BIO-Encoding 2/4505 labels, 0.04%
Test-Data: Prec: 0.799, Rec: 0.805, F1: 0.8022

Scores from epoch with best dev-scores:
  Dev-Score: 0.7925
  Test-Score 0.8022

127.06 sec for evaluation

--------- Epoch 2 -----------
722.50 sec for training (1442.63 total)
-- Poems_v2 --
Wrong BIO-Encoding 2/3861 labels, 0.05%
Wrong BIO-Encoding 2/3861 labels, 0.05%
Dev-Data: Prec: 0.832, Rec: 0.817, F1: 0.8241
Wrong BIO-Encoding 2/4408 labels, 0.05%
Wrong BIO-Encoding 2/4408 labels, 0.05%
Test-Data: Prec: 0.833, Rec: 0.821, F1: 0.8272

Scores from epoch with best dev-scores:
  Dev-Score: 0.8241
  Test-Score 0.8272

94.03 sec for evaluation

--------- Epoch 3 -----------
858.22 sec for training (2300.85 total)
-- Poems_v2 --
Dev-Data: Prec: 0.835, Rec: 0.835, F1: 0.8349
Test-Data: Prec: 0.839, Rec: 0.839, F1: 0.8394

Scores from epoch with best dev-scores:
  Dev-Score: 0.8349
  Test-Score 0.8394

177.43 sec for evaluation

--------- Epoch 4 -----------
37785.13 sec for training (40085.98 total)
-- Poems_v2 --
Dev-Data: Prec: 0.861, Rec: 0.852, F1: 0.8565
Test-Data: Prec: 0.869, Rec: 0.857, F1: 0.8630

Scores from epoch with best dev-scores:
  Dev-Score: 0.8565
  Test-Score 0.8630

96.05 sec for evaluation

--------- Epoch 5 -----------
736.58 sec for training (40822.56 total)
-- Poems_v2 --
Dev-Data: Prec: 0.868, Rec: 0.862, F1: 0.8648
Test-Data: Prec: 0.862, Rec: 0.859, F1: 0.8603

Scores from epoch with best dev-scores:
  Dev-Score: 0.8648
  Test-Score 0.8603

109.02 sec for evaluation

--------- Epoch 6 -----------
760.75 sec for training (41583.30 total)
-- Poems_v2 --
Dev-Data: Prec: 0.866, Rec: 0.866, F1: 0.8660
Wrong BIO-Encoding 1/4464 labels, 0.02%
Wrong BIO-Encoding 1/4464 labels, 0.02%
Test-Data: Prec: 0.873, Rec: 0.872, F1: 0.8725

Scores from epoch with best dev-scores:
  Dev-Score: 0.8660
  Test-Score 0.8725

125.70 sec for evaluation

--------- Epoch 7 -----------
812.84 sec for training (42396.15 total)
-- Poems_v2 --
Dev-Data: Prec: 0.867, Rec: 0.856, F1: 0.8617
Wrong BIO-Encoding 1/4414 labels, 0.02%
Wrong BIO-Encoding 1/4414 labels, 0.02%
Test-Data: Prec: 0.870, Rec: 0.859, F1: 0.8643

Scores from epoch with best dev-scores:
  Dev-Score: 0.8660
  Test-Score 0.8725

144.54 sec for evaluation

--------- Epoch 8 -----------
906.46 sec for training (43302.61 total)
-- Poems_v2 --
Dev-Data: Prec: 0.878, Rec: 0.876, F1: 0.8769
Wrong BIO-Encoding 1/4465 labels, 0.02%
Wrong BIO-Encoding 1/4465 labels, 0.02%
Test-Data: Prec: 0.880, Rec: 0.879, F1: 0.8793

Scores from epoch with best dev-scores:
  Dev-Score: 0.8769
  Test-Score 0.8793

136.78 sec for evaluation

--------- Epoch 9 -----------
934.36 sec for training (44236.97 total)
-- Poems_v2 --
Dev-Data: Prec: 0.876, Rec: 0.881, F1: 0.8786
Test-Data: Prec: 0.874, Rec: 0.884, F1: 0.8788

Scores from epoch with best dev-scores:
  Dev-Score: 0.8786
  Test-Score 0.8788

149.95 sec for evaluation

--------- Epoch 10 -----------
935.93 sec for training (45172.90 total)
-- Poems_v2 --
Dev-Data: Prec: 0.876, Rec: 0.877, F1: 0.8764
Test-Data: Prec: 0.881, Rec: 0.880, F1: 0.8805

Scores from epoch with best dev-scores:
  Dev-Score: 0.8786
  Test-Score 0.8788

147.97 sec for evaluation

--------- Epoch 11 -----------
947.42 sec for training (46120.32 total)
-- Poems_v2 --
Dev-Data: Prec: 0.884, Rec: 0.886, F1: 0.8851
Wrong BIO-Encoding 1/4510 labels, 0.02%
Wrong BIO-Encoding 1/4510 labels, 0.02%
Test-Data: Prec: 0.884, Rec: 0.891, F1: 0.8874

Scores from epoch with best dev-scores:
  Dev-Score: 0.8851
  Test-Score 0.8874

147.02 sec for evaluation

--------- Epoch 12 -----------
995.94 sec for training (47116.26 total)
-- Poems_v2 --
Dev-Data: Prec: 0.885, Rec: 0.880, F1: 0.8821
Test-Data: Prec: 0.886, Rec: 0.880, F1: 0.8830

Scores from epoch with best dev-scores:
  Dev-Score: 0.8851
  Test-Score 0.8874

153.98 sec for evaluation

--------- Epoch 13 -----------
1010.21 sec for training (48126.47 total)
-- Poems_v2 --
Dev-Data: Prec: 0.875, Rec: 0.881, F1: 0.8777
Test-Data: Prec: 0.883, Rec: 0.889, F1: 0.8860

Scores from epoch with best dev-scores:
  Dev-Score: 0.8851
  Test-Score 0.8874

160.24 sec for evaluation

--------- Epoch 14 -----------
1027.44 sec for training (49153.91 total)
-- Poems_v2 --
Dev-Data: Prec: 0.882, Rec: 0.881, F1: 0.8812
Test-Data: Prec: 0.885, Rec: 0.887, F1: 0.8857

Scores from epoch with best dev-scores:
  Dev-Score: 0.8851
  Test-Score 0.8874

153.08 sec for evaluation

--------- Epoch 15 -----------
1027.99 sec for training (50181.91 total)
-- Poems_v2 --
Dev-Data: Prec: 0.892, Rec: 0.889, F1: 0.8903
Test-Data: Prec: 0.889, Rec: 0.889, F1: 0.8890

Scores from epoch with best dev-scores:
  Dev-Score: 0.8903
  Test-Score 0.8890

155.51 sec for evaluation

--------- Epoch 16 -----------
1039.69 sec for training (51221.60 total)
-- Poems_v2 --
Dev-Data: Prec: 0.878, Rec: 0.878, F1: 0.8779
Test-Data: Prec: 0.873, Rec: 0.875, F1: 0.8739

Scores from epoch with best dev-scores:
  Dev-Score: 0.8903
  Test-Score 0.8890

157.08 sec for evaluation

--------- Epoch 17 -----------
1052.10 sec for training (52273.70 total)
-- Poems_v2 --
Dev-Data: Prec: 0.883, Rec: 0.886, F1: 0.8846
Wrong BIO-Encoding 1/4503 labels, 0.02%
Wrong BIO-Encoding 1/4503 labels, 0.02%
Test-Data: Prec: 0.882, Rec: 0.888, F1: 0.8848

Scores from epoch with best dev-scores:
  Dev-Score: 0.8903
  Test-Score 0.8890

160.23 sec for evaluation

--------- Epoch 18 -----------
1041.55 sec for training (53315.26 total)
-- Poems_v2 --
Dev-Data: Prec: 0.892, Rec: 0.889, F1: 0.8901
Test-Data: Prec: 0.889, Rec: 0.887, F1: 0.8882

Scores from epoch with best dev-scores:
  Dev-Score: 0.8903
  Test-Score 0.8890

155.82 sec for evaluation

--------- Epoch 19 -----------
1041.24 sec for training (54356.50 total)
-- Poems_v2 --
Dev-Data: Prec: 0.889, Rec: 0.887, F1: 0.8881
Test-Data: Prec: 0.889, Rec: 0.890, F1: 0.8898

Scores from epoch with best dev-scores:
  Dev-Score: 0.8903
  Test-Score 0.8890

158.27 sec for evaluation

--------- Epoch 20 -----------
1062.71 sec for training (55419.21 total)
-- Poems_v2 --
Dev-Data: Prec: 0.894, Rec: 0.890, F1: 0.8920
Test-Data: Prec: 0.895, Rec: 0.895, F1: 0.8946

Scores from epoch with best dev-scores:
  Dev-Score: 0.8920
  Test-Score 0.8946

162.78 sec for evaluation

--------- Epoch 21 -----------
1077.43 sec for training (56496.63 total)
-- Poems_v2 --
Dev-Data: Prec: 0.888, Rec: 0.884, F1: 0.8860
Test-Data: Prec: 0.891, Rec: 0.889, F1: 0.8905

Scores from epoch with best dev-scores:
  Dev-Score: 0.8920
  Test-Score 0.8946

158.14 sec for evaluation

--------- Epoch 22 -----------
1085.53 sec for training (57582.16 total)
-- Poems_v2 --
Dev-Data: Prec: 0.890, Rec: 0.891, F1: 0.8905
Test-Data: Prec: 0.891, Rec: 0.893, F1: 0.8921

Scores from epoch with best dev-scores:
  Dev-Score: 0.8920
  Test-Score 0.8946

162.17 sec for evaluation

--------- Epoch 23 -----------
1091.02 sec for training (58673.18 total)
-- Poems_v2 --
Dev-Data: Prec: 0.886, Rec: 0.886, F1: 0.8859
Test-Data: Prec: 0.893, Rec: 0.895, F1: 0.8937

Scores from epoch with best dev-scores:
  Dev-Score: 0.8920
  Test-Score 0.8946

165.98 sec for evaluation

--------- Epoch 24 -----------
1117.91 sec for training (59791.09 total)
-- Poems_v2 --
Wrong BIO-Encoding 1/3919 labels, 0.03%
Wrong BIO-Encoding 1/3919 labels, 0.03%
Dev-Data: Prec: 0.894, Rec: 0.892, F1: 0.8931
Test-Data: Prec: 0.895, Rec: 0.895, F1: 0.8952

Scores from epoch with best dev-scores:
  Dev-Score: 0.8931
  Test-Score 0.8952

169.63 sec for evaluation

--------- Epoch 25 -----------
1219.23 sec for training (61010.32 total)
-- Poems_v2 --
Dev-Data: Prec: 0.888, Rec: 0.887, F1: 0.8878
Test-Data: Prec: 0.889, Rec: 0.893, F1: 0.8912

Scores from epoch with best dev-scores:
  Dev-Score: 0.8931
  Test-Score 0.8952

166.40 sec for evaluation
