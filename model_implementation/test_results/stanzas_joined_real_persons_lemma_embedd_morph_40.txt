Using existent pickle file: pkl/Stanzas_joined_real_persons_word_2_vec_lemma.txt.pkl
--- Stanzas_joined_real_persons ---
238561 train sentences
42098 dev sentences
49527 test sentences
Pad words to uniform length for characters embeddings
Words padded to 25 characters
LSTM-Size: [100, 100]
_____________________________________________________________________________________________________________________________
Layer (type)                             Output Shape               Param #        Connected to                              
=============================================================================================================================
char_input (InputLayer)                  (None, None, 25)           0                                                        
_____________________________________________________________________________________________________________________________
char_emd (TimeDistributed)               (None, None, 25, 25)       2375           char_input[0][0]                          
_____________________________________________________________________________________________________________________________
words_input (InputLayer)                 (None, None)               0                                                        
_____________________________________________________________________________________________________________________________
morph_input (InputLayer)                 (None, None)               0                                                        
_____________________________________________________________________________________________________________________________
char_cnn (TimeDistributed)               (None, None, 25, 30)       2280           char_emd[0][0]                            
_____________________________________________________________________________________________________________________________
word_embeddings (Embedding)              (None, None, 100)          18622400       words_input[0][0]                         
_____________________________________________________________________________________________________________________________
morph_emebddings (Embedding)             (None, None, 40)           145680         morph_input[0][0]                         
_____________________________________________________________________________________________________________________________
char_pooling (TimeDistributed)           (None, None, 30)           0              char_cnn[0][0]                            
_____________________________________________________________________________________________________________________________
concatenate_1 (Concatenate)              (None, None, 170)          0              word_embeddings[0][0]                     
                                                                                   morph_emebddings[0][0]                    
                                                                                   char_pooling[0][0]                        
_____________________________________________________________________________________________________________________________
shared_varLSTM_1 (Bidirectional)         (None, None, 200)          216800         concatenate_1[0][0]                       
_____________________________________________________________________________________________________________________________
shared_varLSTM_2 (Bidirectional)         (None, None, 200)          240800         shared_varLSTM_1[0][0]                    
_____________________________________________________________________________________________________________________________
Stanzas_joined_real_persons_hidden_lin_l (None, None, 9)            1809           shared_varLSTM_2[0][0]                    
_____________________________________________________________________________________________________________________________
Stanzas_joined_real_persons_crf (ChainCR (None, None, 9)            99             Stanzas_joined_real_persons_hidden_lin_lay
=============================================================================================================================
Total params: 19,232,243
Trainable params: 609,843
Non-trainable params: 18,622,400
_____________________________________________________________________________________________________________________________

--------- Epoch 1 -----------
1029.33 sec for training (1029.33 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.854, Rec: 0.843, F1: 0.8483
Test-Data: Prec: 0.868, Rec: 0.856, F1: 0.8619

Scores from epoch with best dev-scores:
  Dev-Score: 0.8483
  Test-Score 0.8619

203.28 sec for evaluation

--------- Epoch 2 -----------
1201.56 sec for training (2230.90 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.876, Rec: 0.885, F1: 0.8808
Test-Data: Prec: 0.884, Rec: 0.891, F1: 0.8875

Scores from epoch with best dev-scores:
  Dev-Score: 0.8808
  Test-Score 0.8875

228.04 sec for evaluation

--------- Epoch 3 -----------
1259.06 sec for training (3489.95 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.903, Rec: 0.897, F1: 0.8998
Test-Data: Prec: 0.909, Rec: 0.901, F1: 0.9050

Scores from epoch with best dev-scores:
  Dev-Score: 0.8998
  Test-Score 0.9050

228.10 sec for evaluation

--------- Epoch 4 -----------
1386.59 sec for training (4876.54 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.912, Rec: 0.913, F1: 0.9126
Test-Data: Prec: 0.916, Rec: 0.915, F1: 0.9154

Scores from epoch with best dev-scores:
  Dev-Score: 0.9126
  Test-Score 0.9154

258.11 sec for evaluation

--------- Epoch 5 -----------
1459.16 sec for training (6335.69 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.925, Rec: 0.922, F1: 0.9237
Test-Data: Prec: 0.927, Rec: 0.924, F1: 0.9252

Scores from epoch with best dev-scores:
  Dev-Score: 0.9237
  Test-Score 0.9252

251.43 sec for evaluation

--------- Epoch 6 -----------
1542.82 sec for training (7878.51 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.926, Rec: 0.918, F1: 0.9219
Test-Data: Prec: 0.929, Rec: 0.919, F1: 0.9240

Scores from epoch with best dev-scores:
  Dev-Score: 0.9237
  Test-Score 0.9252

262.53 sec for evaluation

--------- Epoch 7 -----------
1573.54 sec for training (9452.05 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.928, Rec: 0.931, F1: 0.9296
Test-Data: Prec: 0.929, Rec: 0.932, F1: 0.9304

Scores from epoch with best dev-scores:
  Dev-Score: 0.9296
  Test-Score 0.9304

274.72 sec for evaluation

--------- Epoch 8 -----------
1682.89 sec for training (11134.94 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.930, Rec: 0.934, F1: 0.9323
Test-Data: Prec: 0.932, Rec: 0.936, F1: 0.9341

Scores from epoch with best dev-scores:
  Dev-Score: 0.9323
  Test-Score 0.9341

254.94 sec for evaluation

--------- Epoch 9 -----------
1618.69 sec for training (12753.63 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.932, Rec: 0.936, F1: 0.9342
Test-Data: Prec: 0.935, Rec: 0.939, F1: 0.9371

Scores from epoch with best dev-scores:
  Dev-Score: 0.9342
  Test-Score 0.9371

268.75 sec for evaluation

--------- Epoch 10 -----------
1661.06 sec for training (14414.69 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.937, Rec: 0.934, F1: 0.9352
Test-Data: Prec: 0.940, Rec: 0.935, F1: 0.9373

Scores from epoch with best dev-scores:
  Dev-Score: 0.9352
  Test-Score 0.9373

271.95 sec for evaluation

--------- Epoch 11 -----------
1684.00 sec for training (16098.69 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 1/11497 labels, 0.01%
Wrong BIO-Encoding 1/11497 labels, 0.01%
Dev-Data: Prec: 0.930, Rec: 0.936, F1: 0.9331
Test-Data: Prec: 0.937, Rec: 0.941, F1: 0.9389

Scores from epoch with best dev-scores:
  Dev-Score: 0.9352
  Test-Score 0.9373

281.07 sec for evaluation

--------- Epoch 12 -----------
1621.91 sec for training (17720.60 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.942, Rec: 0.940, F1: 0.9410
Test-Data: Prec: 0.943, Rec: 0.942, F1: 0.9422

Scores from epoch with best dev-scores:
  Dev-Score: 0.9410
  Test-Score 0.9422

268.97 sec for evaluation

--------- Epoch 13 -----------
1679.87 sec for training (19400.47 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 1/11469 labels, 0.01%
Wrong BIO-Encoding 1/11469 labels, 0.01%
Dev-Data: Prec: 0.932, Rec: 0.936, F1: 0.9344
Test-Data: Prec: 0.935, Rec: 0.938, F1: 0.9365

Scores from epoch with best dev-scores:
  Dev-Score: 0.9410
  Test-Score 0.9422

275.16 sec for evaluation

--------- Epoch 14 -----------
1728.68 sec for training (21129.14 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.939, Rec: 0.940, F1: 0.9399
Test-Data: Prec: 0.943, Rec: 0.944, F1: 0.9432

Scores from epoch with best dev-scores:
  Dev-Score: 0.9410
  Test-Score 0.9422

284.17 sec for evaluation

--------- Epoch 15 -----------
1765.51 sec for training (22894.65 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.935, Rec: 0.942, F1: 0.9385
Test-Data: Prec: 0.943, Rec: 0.946, F1: 0.9446

Scores from epoch with best dev-scores:
  Dev-Score: 0.9410
  Test-Score 0.9422

280.11 sec for evaluation

--------- Epoch 16 -----------
1801.41 sec for training (24696.06 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.943, Rec: 0.946, F1: 0.9445
Test-Data: Prec: 0.946, Rec: 0.949, F1: 0.9471

Scores from epoch with best dev-scores:
  Dev-Score: 0.9445
  Test-Score 0.9471

288.47 sec for evaluation

--------- Epoch 17 -----------
1827.77 sec for training (26523.84 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.942, Rec: 0.943, F1: 0.9423
Test-Data: Prec: 0.948, Rec: 0.949, F1: 0.9484

Scores from epoch with best dev-scores:
  Dev-Score: 0.9445
  Test-Score 0.9471

287.89 sec for evaluation

--------- Epoch 18 -----------
1873.93 sec for training (28397.77 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.939, Rec: 0.941, F1: 0.9401
Test-Data: Prec: 0.948, Rec: 0.947, F1: 0.9475

Scores from epoch with best dev-scores:
  Dev-Score: 0.9445
  Test-Score 0.9471

300.33 sec for evaluation

--------- Epoch 19 -----------
1870.71 sec for training (30268.48 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 1/11490 labels, 0.01%
Wrong BIO-Encoding 1/11490 labels, 0.01%
Dev-Data: Prec: 0.942, Rec: 0.947, F1: 0.9446
Test-Data: Prec: 0.944, Rec: 0.948, F1: 0.9458

Scores from epoch with best dev-scores:
  Dev-Score: 0.9446
  Test-Score 0.9458

297.04 sec for evaluation

--------- Epoch 20 -----------
1907.64 sec for training (32176.12 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.940, Rec: 0.945, F1: 0.9424
Test-Data: Prec: 0.945, Rec: 0.948, F1: 0.9462

Scores from epoch with best dev-scores:
  Dev-Score: 0.9446
  Test-Score 0.9458

302.63 sec for evaluation

--------- Epoch 21 -----------
1953.94 sec for training (34130.06 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 2/11502 labels, 0.02%
Wrong BIO-Encoding 2/11502 labels, 0.02%
Dev-Data: Prec: 0.939, Rec: 0.945, F1: 0.9420
Test-Data: Prec: 0.945, Rec: 0.951, F1: 0.9479

Scores from epoch with best dev-scores:
  Dev-Score: 0.9446
  Test-Score 0.9458

309.87 sec for evaluation

--------- Epoch 22 -----------
1968.00 sec for training (36098.07 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.942, Rec: 0.946, F1: 0.9441
Test-Data: Prec: 0.948, Rec: 0.949, F1: 0.9488

Scores from epoch with best dev-scores:
  Dev-Score: 0.9446
  Test-Score 0.9458

313.66 sec for evaluation

--------- Epoch 23 -----------
2004.07 sec for training (38102.14 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.942, Rec: 0.947, F1: 0.9445
Test-Data: Prec: 0.946, Rec: 0.951, F1: 0.9487

Scores from epoch with best dev-scores:
  Dev-Score: 0.9446
  Test-Score 0.9458

354.06 sec for evaluation

--------- Epoch 24 -----------
2183.56 sec for training (40285.70 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.945, Rec: 0.947, F1: 0.9457
Test-Data: Prec: 0.949, Rec: 0.949, F1: 0.9490

Scores from epoch with best dev-scores:
  Dev-Score: 0.9457
  Test-Score 0.9490

323.76 sec for evaluation

--------- Epoch 25 -----------
1999.05 sec for training (42284.75 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 4/11441 labels, 0.03%
Wrong BIO-Encoding 4/11441 labels, 0.03%
Dev-Data: Prec: 0.946, Rec: 0.947, F1: 0.9467
Test-Data: Prec: 0.949, Rec: 0.947, F1: 0.9482

Scores from epoch with best dev-scores:
  Dev-Score: 0.9467
  Test-Score 0.9482

329.78 sec for evaluation
