Generate new embeddings files for a dataset
Read file: word_2_vec_lemma.txt.gz
Added words: 93
:: Transform Stanzas_joined_real_persons dataset ::
:: Create Train Matrix ::
Unknown-Tokens: 0.04%
:: Create Dev Matrix ::
Unknown-Tokens: 0.04%
:: Create Test Matrix ::
Unknown-Tokens: 0.04%
DONE - Embeddings file saved: pkl/Stanzas_joined_real_persons_word_2_vec_lemma.txt.pkl
--- Stanzas_joined_real_persons ---
238561 train sentences
42098 dev sentences
49527 test sentences
Pad words to uniform length for characters embeddings
Words padded to 25 characters
LSTM-Size: [100, 100]
_____________________________________________________________________________________________________________________________
Layer (type)                             Output Shape               Param #        Connected to                              
=============================================================================================================================
char_input (InputLayer)                  (None, None, 25)           0                                                        
_____________________________________________________________________________________________________________________________
char_emd (TimeDistributed)               (None, None, 25, 25)       2375           char_input[0][0]                          
_____________________________________________________________________________________________________________________________
words_input (InputLayer)                 (None, None)               0                                                        
_____________________________________________________________________________________________________________________________
morph_input (InputLayer)                 (None, None)               0                                                        
_____________________________________________________________________________________________________________________________
char_cnn (TimeDistributed)               (None, None, 25, 30)       2280           char_emd[0][0]                            
_____________________________________________________________________________________________________________________________
word_embeddings (Embedding)              (None, None, 100)          18622400       words_input[0][0]                         
_____________________________________________________________________________________________________________________________
morph_emebddings (Embedding)             (None, None, 10)           36420          morph_input[0][0]                         
_____________________________________________________________________________________________________________________________
char_pooling (TimeDistributed)           (None, None, 30)           0              char_cnn[0][0]                            
_____________________________________________________________________________________________________________________________
concatenate_1 (Concatenate)              (None, None, 140)          0              word_embeddings[0][0]                     
                                                                                   morph_emebddings[0][0]                    
                                                                                   char_pooling[0][0]                        
_____________________________________________________________________________________________________________________________
shared_varLSTM_1 (Bidirectional)         (None, None, 200)          192800         concatenate_1[0][0]                       
_____________________________________________________________________________________________________________________________
shared_varLSTM_2 (Bidirectional)         (None, None, 200)          240800         shared_varLSTM_1[0][0]                    
_____________________________________________________________________________________________________________________________
Stanzas_joined_real_persons_hidden_lin_l (None, None, 9)            1809           shared_varLSTM_2[0][0]                    
_____________________________________________________________________________________________________________________________
Stanzas_joined_real_persons_crf (ChainCR (None, None, 9)            99             Stanzas_joined_real_persons_hidden_lin_lay
=============================================================================================================================
Total params: 19,098,983
Trainable params: 476,583
Non-trainable params: 18,622,400
_____________________________________________________________________________________________________________________________

--------- Epoch 1 -----------
968.12 sec for training (968.12 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.853, Rec: 0.844, F1: 0.8485
Test-Data: Prec: 0.871, Rec: 0.860, F1: 0.8652

Scores from epoch with best dev-scores:
  Dev-Score: 0.8485
  Test-Score 0.8652

132.61 sec for evaluation

--------- Epoch 2 -----------
1019.28 sec for training (1987.40 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.886, Rec: 0.881, F1: 0.8837
Test-Data: Prec: 0.891, Rec: 0.887, F1: 0.8891

Scores from epoch with best dev-scores:
  Dev-Score: 0.8837
  Test-Score 0.8891

196.96 sec for evaluation

--------- Epoch 3 -----------
1051.79 sec for training (3039.19 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.900, Rec: 0.902, F1: 0.9013
Test-Data: Prec: 0.903, Rec: 0.905, F1: 0.9042

Scores from epoch with best dev-scores:
  Dev-Score: 0.9013
  Test-Score 0.9042

217.10 sec for evaluation

--------- Epoch 4 -----------
1133.84 sec for training (4173.03 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.912, Rec: 0.915, F1: 0.9136
Test-Data: Prec: 0.918, Rec: 0.920, F1: 0.9189

Scores from epoch with best dev-scores:
  Dev-Score: 0.9136
  Test-Score 0.9189

217.24 sec for evaluation

--------- Epoch 5 -----------
1369.44 sec for training (5542.47 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.920, Rec: 0.918, F1: 0.9186
Test-Data: Prec: 0.923, Rec: 0.921, F1: 0.9222

Scores from epoch with best dev-scores:
  Dev-Score: 0.9186
  Test-Score 0.9222

241.14 sec for evaluation

--------- Epoch 6 -----------
1300.31 sec for training (6842.79 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.925, Rec: 0.929, F1: 0.9272
Test-Data: Prec: 0.928, Rec: 0.931, F1: 0.9292

Scores from epoch with best dev-scores:
  Dev-Score: 0.9272
  Test-Score 0.9292

235.86 sec for evaluation

--------- Epoch 7 -----------
1361.91 sec for training (8204.70 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.927, Rec: 0.928, F1: 0.9278
Test-Data: Prec: 0.934, Rec: 0.935, F1: 0.9344

Scores from epoch with best dev-scores:
  Dev-Score: 0.9278
  Test-Score 0.9344

221.88 sec for evaluation

--------- Epoch 8 -----------
1275.66 sec for training (9480.36 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.934, Rec: 0.931, F1: 0.9327
Test-Data: Prec: 0.938, Rec: 0.934, F1: 0.9358

Scores from epoch with best dev-scores:
  Dev-Score: 0.9327
  Test-Score 0.9358

238.34 sec for evaluation

--------- Epoch 9 -----------
1371.07 sec for training (10851.42 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.933, Rec: 0.930, F1: 0.9316
Test-Data: Prec: 0.940, Rec: 0.936, F1: 0.9379

Scores from epoch with best dev-scores:
  Dev-Score: 0.9327
  Test-Score 0.9358

242.78 sec for evaluation

--------- Epoch 10 -----------
1476.53 sec for training (12327.95 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.931, Rec: 0.934, F1: 0.9328
Test-Data: Prec: 0.938, Rec: 0.940, F1: 0.9387

Scores from epoch with best dev-scores:
  Dev-Score: 0.9328
  Test-Score 0.9387

245.41 sec for evaluation

--------- Epoch 11 -----------
1509.77 sec for training (13837.72 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 2/11506 labels, 0.02%
Wrong BIO-Encoding 2/11506 labels, 0.02%
Dev-Data: Prec: 0.934, Rec: 0.941, F1: 0.9374
Test-Data: Prec: 0.939, Rec: 0.945, F1: 0.9419

Scores from epoch with best dev-scores:
  Dev-Score: 0.9374
  Test-Score 0.9419

239.85 sec for evaluation

--------- Epoch 12 -----------
1521.08 sec for training (15358.81 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.939, Rec: 0.945, F1: 0.9420
Test-Data: Prec: 0.942, Rec: 0.948, F1: 0.9450

Scores from epoch with best dev-scores:
  Dev-Score: 0.9420
  Test-Score 0.9450

255.21 sec for evaluation

--------- Epoch 13 -----------
1507.84 sec for training (16866.65 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.937, Rec: 0.943, F1: 0.9398
Test-Data: Prec: 0.942, Rec: 0.948, F1: 0.9449

Scores from epoch with best dev-scores:
  Dev-Score: 0.9420
  Test-Score 0.9450

250.52 sec for evaluation

--------- Epoch 14 -----------
1530.85 sec for training (18397.49 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.937, Rec: 0.944, F1: 0.9403
Wrong BIO-Encoding 1/13823 labels, 0.01%
Wrong BIO-Encoding 1/13823 labels, 0.01%
Test-Data: Prec: 0.941, Rec: 0.948, F1: 0.9447

Scores from epoch with best dev-scores:
  Dev-Score: 0.9420
  Test-Score 0.9450

254.11 sec for evaluation

--------- Epoch 15 -----------
1553.12 sec for training (19950.62 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.939, Rec: 0.941, F1: 0.9400
Test-Data: Prec: 0.948, Rec: 0.948, F1: 0.9478

Scores from epoch with best dev-scores:
  Dev-Score: 0.9420
  Test-Score 0.9450

251.61 sec for evaluation

--------- Epoch 16 -----------
1581.56 sec for training (21532.18 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.941, Rec: 0.942, F1: 0.9417
Test-Data: Prec: 0.944, Rec: 0.944, F1: 0.9437

Scores from epoch with best dev-scores:
  Dev-Score: 0.9420
  Test-Score 0.9450

276.41 sec for evaluation

--------- Epoch 17 -----------
1665.14 sec for training (23197.32 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.939, Rec: 0.941, F1: 0.9401
Test-Data: Prec: 0.945, Rec: 0.945, F1: 0.9450

Scores from epoch with best dev-scores:
  Dev-Score: 0.9420
  Test-Score 0.9450

268.90 sec for evaluation

--------- Epoch 18 -----------
1673.98 sec for training (24871.31 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.942, Rec: 0.943, F1: 0.9426
Test-Data: Prec: 0.947, Rec: 0.949, F1: 0.9479

Scores from epoch with best dev-scores:
  Dev-Score: 0.9426
  Test-Score 0.9479

273.89 sec for evaluation

--------- Epoch 19 -----------
1720.62 sec for training (26591.92 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 4/11475 labels, 0.03%
Wrong BIO-Encoding 4/11475 labels, 0.03%
Dev-Data: Prec: 0.943, Rec: 0.947, F1: 0.9449
Test-Data: Prec: 0.946, Rec: 0.949, F1: 0.9475

Scores from epoch with best dev-scores:
  Dev-Score: 0.9449
  Test-Score 0.9475

281.55 sec for evaluation

--------- Epoch 20 -----------
1691.76 sec for training (28283.69 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.941, Rec: 0.946, F1: 0.9435
Test-Data: Prec: 0.946, Rec: 0.951, F1: 0.9483

Scores from epoch with best dev-scores:
  Dev-Score: 0.9449
  Test-Score 0.9475

280.49 sec for evaluation

--------- Epoch 21 -----------
1683.11 sec for training (29966.79 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 2/11455 labels, 0.02%
Wrong BIO-Encoding 2/11455 labels, 0.02%
Dev-Data: Prec: 0.946, Rec: 0.948, F1: 0.9470
Test-Data: Prec: 0.952, Rec: 0.952, F1: 0.9517

Scores from epoch with best dev-scores:
  Dev-Score: 0.9470
  Test-Score 0.9517

286.63 sec for evaluation

--------- Epoch 22 -----------
1721.58 sec for training (31688.38 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 3/11431 labels, 0.03%
Wrong BIO-Encoding 3/11431 labels, 0.03%
Dev-Data: Prec: 0.946, Rec: 0.947, F1: 0.9465
Wrong BIO-Encoding 1/13687 labels, 0.01%
Wrong BIO-Encoding 1/13687 labels, 0.01%
Test-Data: Prec: 0.952, Rec: 0.949, F1: 0.9506

Scores from epoch with best dev-scores:
  Dev-Score: 0.9470
  Test-Score 0.9517

288.17 sec for evaluation

--------- Epoch 23 -----------
1755.58 sec for training (33443.96 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.950, Rec: 0.951, F1: 0.9502
Test-Data: Prec: 0.953, Rec: 0.953, F1: 0.9532

Scores from epoch with best dev-scores:
  Dev-Score: 0.9502
  Test-Score 0.9532

300.41 sec for evaluation

--------- Epoch 24 -----------
1779.38 sec for training (35223.34 total)
-- Stanzas_joined_real_persons --
Wrong BIO-Encoding 4/11479 labels, 0.03%
Wrong BIO-Encoding 4/11479 labels, 0.03%
Dev-Data: Prec: 0.944, Rec: 0.949, F1: 0.9463
Wrong BIO-Encoding 1/13790 labels, 0.01%
Wrong BIO-Encoding 1/13790 labels, 0.01%
Test-Data: Prec: 0.949, Rec: 0.954, F1: 0.9513

Scores from epoch with best dev-scores:
  Dev-Score: 0.9502
  Test-Score 0.9532

294.34 sec for evaluation

--------- Epoch 25 -----------
1797.88 sec for training (37021.22 total)
-- Stanzas_joined_real_persons --
Dev-Data: Prec: 0.948, Rec: 0.951, F1: 0.9496
Test-Data: Prec: 0.950, Rec: 0.954, F1: 0.9519

Scores from epoch with best dev-scores:
  Dev-Score: 0.9502
  Test-Score 0.9532

305.76 sec for evaluation
