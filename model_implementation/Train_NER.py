from __future__ import print_function
import os
import logging
import sys
from BiLSTM import BiLSTM
from preprocessing import perpareDataset, loadDatasetPickle

from keras import backend as K

# :: Change into the working dir of the script ::
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

# :: Logging level ::
loggingLevel = logging.INFO
logger = logging.getLogger()
logger.setLevel(loggingLevel)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(loggingLevel)
formatter = logging.Formatter('%(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


######################################################
#
# Data preprocessing
#
######################################################
datasets = {
    'Stanzas_newer_version':                                   #Name of the dataset
        {'columns': {0:'position', 1:'real_tokens', 2:'morph', 3:'tokens', 4:'NER_BIO'},    #CoNLL format for the input data. Column 1 contains tokens, column 2 contains NER information using BIO encoding
         'label': 'NER_BIO',                      #Which column we like to predict
         'evaluate': True,                        #Should we evaluate on this task? Set true always for single task setups
         'commentSymbol': None}                   #Lines in the input data starting with this string will be skipped. Can be used to skip comments
}
# :: Path on your computer to the word embeddings. Embeddings by Reimers et al. will be downloaded automatically ::

embeddingsPath_lemma = 'word_2_vec_lemma.txt.gz'
#embeddingsPath_token = 'word_2_vec_token.txt.gz'

# :: Prepares the dataset to be used with the LSTM-network. Creates and stores cPickle files in the pkl/ folder ::
pickleFile_lemma = perpareDataset(embeddingsPath_lemma, datasets)
#pickleFile_token = perpareDataset(embeddingsPath_token, datasets)


######################################################
#
# The training of the network starts here
#
######################################################hm


#Load the embeddings and the dataset
#embeddings_token, mappings_token, data_token = loadDatasetPickle(pickleFile_token)
embeddings, mappings, data = loadDatasetPickle(pickleFile_lemma)

# Some network hyperparameters
params = {'classifier': ['CRF'], 'LSTM-Size': [100, 100], 'dropout': (0.05, 0.05), 'charEmbeddings': 'CNN', 'charEmbeddingsSize': 25,
          'featureNames': ['tokens', 'morph'], 'addFeatureDimensions': 10, 'earlyStopping': 15}

model = BiLSTM(params)
#model.setMappings(mappings, embeddings, embeddings_token)
model.setMappings(mappings, embeddings)

model.setDataset(datasets, data)
model.modelSavePath = "./models/[ModelName]_[DevScore]_[TestScore]_[Epoch].h5"
model.fit(epochs=25)



