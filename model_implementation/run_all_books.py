import os

input_files = []
output_files = []

for inFile in os.listdir("./inputs/all_books"):
    input_files.append("./inputs/all_books" + "/" + inFile)
    output_files.append("./outputs/all_books" + "/" + inFile.split('.')[0] + ".txt")
    
for i in range(len(input_files)):
    print("Processing file: " + input_files[i])
    os.system(f'python ./RunModel_CoNLL_Format.py ./models/Stanzas_joined_real_persons_0.9502_0.9532_23.h5 {input_files[i]} {output_files[i]}')
    print("Done processing file: " + input_files[i])
    
