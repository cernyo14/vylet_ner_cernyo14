import sys
from itertools import tee

def correct_real_mystic_names(predicted_file: str):
    """
    Changes REAL to REAL PERSON and MYSTIC to MYSTIC PERSON
    param: predicted_file: file in which we do this correction
    """
    with open(predicted_file, "r", encoding="utf-8") as read_file, open(predicted_file.replace(".txt", "_corrected") + ".txt", "w", encoding="utf-8") as write_file:
        for line in read_file:
            line_row = line.strip().split("\t")
            
            if line_row[-1] == "B-REAL":
                line_row[-1] = "B-REAL PERSON"
            elif line_row[-1] == "I-REAL":
                line_row[-1] = "I-REAL PERSON"
            elif line_row[-1] == "B-MYSTIC":
                line_row[-1] = "B-MYSTIC PERSON"
                
            write_file.write("\t".join(line_row) + "\n")
    

def check_next_words(corr_data_iter, pred_data_iter):
    """
    Checks accuracy of multi-word entities
    """
    try:
        corr_row = next(corr_data_iter).strip().split("\t")
        pred_row = next(pred_data_iter).strip().split("\t")
    except StopIteration:
        return True
    
    if corr_row[-1] and pred_row[-1]:
        if corr_row[-1][0] != pred_row[-1][0]:
            return False
    
    if pred_row[-1].startswith("I"):
        if corr_row[-1] == pred_row[-1]:
            return check_next_words(corr_data_iter, pred_data_iter) 
            
        elif pred_row[-1] == "I-REAL" and corr_row[-1] == "I-REAL PERSON":
            return check_next_words(corr_data_iter, pred_data_iter)  
            
        elif pred_row[-1] == "I-MYSTIC" and corr_row[-1] == "I-MYSTIC PERSON":
            return check_next_words(corr_data_iter, pred_data_iter)
        
        else:
            return False
        
    return True
    
    
    

def entity_accuracy(correct_file: str, predicted_file: str, print_out: bool = False):
    '''
    Calculates the accuracy of the model.
    '''
    # Open files
    with open(correct_file, "r", encoding="utf-8") as f1, open(predicted_file, "r", encoding="utf-8") as f2:
        # Read data from files
        corr_data = f1.readlines()
        pred_data = f2.readlines()

        # Initialize counters
        correct_entities = 0
        total_entities = 0
        
        correct_word_ents = 0
        correct_words = 0
        total_words = 0

        if len(corr_data) != len(pred_data):
            print("Files have different number of lines")
            return
        
        # Loop through the data
        corr_data_iter = iter(corr_data)
        pred_data_iter = iter(pred_data)
        while True:
            # Split each row by tab
            try:
                corr_row = next(corr_data_iter).strip().split("\t")
                pred_row = next(pred_data_iter).strip().split("\t")
            except StopIteration:
                break

            # Check if the last column starts with "B"
            if pred_row[-1].startswith("B"):
                total_entities += 1
                corr_data_iter, corr_data_iter_cpy = tee(corr_data_iter)
                pred_data_iter, pred_data_iter_cpy = tee(pred_data_iter)
                
                # Check if the predicted entity matches the correct entity
                if corr_row[-1] == pred_row[-1]:
                    if check_next_words(corr_data_iter_cpy, pred_data_iter_cpy):
                        correct_entities += 1
                        
                elif pred_row[-1] == "B-REAL" and corr_row[-1] == "B-REAL PERSON":
                    if check_next_words(corr_data_iter_cpy, pred_data_iter_cpy):
                        correct_entities += 1
                    
                elif pred_row[-1] == "B-MYSTIC" and corr_row[-1] == "B-MYSTIC PERSON":
                    if check_next_words(corr_data_iter_cpy, pred_data_iter_cpy):
                        correct_entities += 1
                        
                else :
                    if print_out:
                        print(corr_row, pred_row)
                    
                    
            if corr_row[-1] and pred_row[-1]:
                total_words += 1
                if corr_row[-1][0] == pred_row[-1][0]:
                    correct_word_ents += 1
                    
                    if corr_row[-1] == pred_row[-1]:
                        correct_words += 1  
            
                    

        # Calculate accuracy
        total_accuracy = correct_words / total_words
        accuracy_entities = correct_entities / total_entities
        accuracy_words = correct_word_ents / total_words


        # Print results
        print(f"Total words: {total_words}")
        print(f"Correctly predicted words: {correct_words}")
        print(f"Total Accuracy: {total_accuracy:.5f}\n")
        
        print(f"Correctly predicted if word is an entity (category not being considered): {correct_word_ents}")
        print(f"Accuracy: {accuracy_words:.5f}\n")
        
        print(f"Total entities: {total_entities}")
        print(f"Correctly predicted entities: {correct_entities}")
        print(f"Accuracy: {accuracy_entities:.5f}\n")
        
        
def entity_accuracy_persons_same(correct_file: str, predicted_file: str, print_out: bool = False):
    '''
    Calculates the accuracy of the model when the categories "Person" and "Real Person" are viewed as the same category.
    '''
    # Open files
    with open(correct_file, "r", encoding="utf-8") as f1, open(predicted_file, "r", encoding="utf-8") as f2:
        # Read data from files
        corr_data = f1.readlines()
        pred_data = f2.readlines()

        # Initialize counters
        correct_entities_persons_same = 0
        total_entities = 0

        if len(corr_data) != len(pred_data):
            print("Files have different number of lines")
            return
        
        # Loop through the data
        corr_data_iter = iter(corr_data)
        pred_data_iter = iter(pred_data)
        while True:
            # Split each row by tab
            try:
                corr_row = next(corr_data_iter).strip().split("\t")
                pred_row = next(pred_data_iter).strip().split("\t")
            except StopIteration:
                break

            # Check if the last column starts with "B"
            if pred_row[-1].startswith("B"):
                total_entities += 1
                corr_data_iter, corr_data_iter_cpy = tee(corr_data_iter)
                pred_data_iter, pred_data_iter_cpy = tee(pred_data_iter)
                
                # Check if the predicted entity matches the correct entity
                if corr_row[-1] == pred_row[-1]:
                    if check_next_words(corr_data_iter_cpy, pred_data_iter_cpy):
                        correct_entities_persons_same += 1
                        
                elif pred_row[-1] == "B-PERSON" or pred_row[-1] == "B-REAL":
                    if corr_row[-1] == "B-PERSON" or corr_row[-1] == "B-REAL PERSON":
                        if check_next_words(corr_data_iter_cpy, pred_data_iter_cpy):
                            correct_entities_persons_same += 1
                        
                else :
                    if print_out:
                        print(corr_row, pred_row)
                    
            
        accuracy_with_persons_same = correct_entities_persons_same / total_entities
        
        print(f"Total entities: {total_entities}")
        print(f"Correctly predicted entities (Entities Person and Real Person viewed as same category): {correct_entities_persons_same}")
        print(f"Accuracy: {accuracy_with_persons_same:.5f}\n")
    
if __name__ == "__main__":
    #entity_accuracy(sys.argv[1], sys.argv[2])
    correct_real_mystic_names(sys.argv[1])
    #entity_accuracy_persons_same("./model/data/Stanzas/dev.txt", "./model/output_stanza_dev_1.txt")
