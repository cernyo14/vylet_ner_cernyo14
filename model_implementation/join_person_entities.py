import sys

def join_person_entities(infile: str, outfile: str):
    '''
    Joins REAL PERSON and MYSTIC PERSON entities to PERSON from infile and writes to outfile
    '''    
    with open(infile, 'r', encoding="utf-8") as f1:
        lines = f1.readlines()
    
    print("Infile read successfully now joining PERSON entities and writing to outfile")

    with open(outfile, 'w', encoding="utf-8") as f2:
        res = ""
        cnt = 0
        for line in lines:
            line_arr = line.strip().split("\t")

            if len(line_arr) < 2:
                res += "\n"
                continue
            
            if line_arr[-1] == "B-REAL PERSON":
                line_arr[-1] = "B-PERSON"
            
            # join mystic person with person
            # elif line_arr[-1] == "B-MYSTIC PERSON":
            #     line_arr[-1] = "B-PERSON"
            
            # join mystic person with person      
            # elif line_arr[-1] == "I-MYSTIC PERSON":
            #    line_arr[-1] = "I-PERSON"
            
            
            elif line_arr[-1] == "I-REAL PERSON":
                line_arr[-1] = "I-PERSON"

            res += "\t".join(line_arr) + "\n"

            cnt += 1
            if cnt % 100000 == 0:
                print(cnt/100000, "100k lines processed")
                f2.write(res)
                res = ""
                
        f2.write(res)
        print("Entities joined successfully now writing to outfile")
        
if __name__ == "__main__":
    join_person_entities(sys.argv[1], sys.argv[2])
    
    