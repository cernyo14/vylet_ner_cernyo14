import argparse
import numpy as np


def get_embedding(embedding_file: str, word: str):
    with open(embedding_file, 'r', encoding='utf-8') as f:
        for line in f:
            parts = line.split()
            
            if len(parts) < 2:
                continue
            
            if word != parts[0]:
                continue
            
            return [float(x) for x in parts[1:]]
    
    raise ValueError(f"Word {word} was not found in the embedding file.") 

def process_input_file(embedding_file: str, input_file: str, output_file: str):
    final_embeddings = []
    
    with open(input_file, "r", encoding="utf-8") as in_file:
        empty_line_count = sum(1 for line in in_file if line == "\n")
    

    with open(input_file, "r", encoding="utf-8") as in_file:
        line_embedd = []
        sentences_covered = 0
        
        for line in in_file:
            if line == "\n":
                final_embeddings.append(line_embedd)
                line_embedd = []
                
                sentences_covered += 1
                if sentences_covered < 1000 and sentences_covered % 100 == 0:
                    print(f"Embedded {sentences_covered} sentences out of {empty_line_count}.")
                if sentences_covered % 1000 == 0:
                    print(f"Embedded {sentences_covered} sentences out of {empty_line_count}.")
                    
                continue
            
            line_embedd.append(get_embedding(embedding_file, line.split()[0]))
    

    np.savez(output_file, *final_embeddings)    

def main():
    """
    Usage: python embeddings_file input_file(training data) output_file
    Text from input file will be embedded and saved as .npz file ([sentences x words x word_embeddings])
    Example: py embedding_to_npz.py word_2_vec_lemma.txt/word_2_vec_lemma.txt inputs/stanzas_newer_version_train.conll word_2_vec_lemma.npz
    """
    #process_input_file("model_implementation/word_2_vec_lemma.txt/word_2_vec_lemma.txt", "model_implementation/inputs/stanzas_new_version_dev.conll", "model_implementation/word_2_vec_lemma_dev.npz")
    process_input_file("src/models_comparison/hand_tagged_test_files/cs_word_2_vec_lemma.txt/cs_word_2_vec_lemma.txt", "src/models_comparison/hand_tagged_test_files/cs_true_lemma.txt", "src/models_comparison/hand_tagged_test_files/cs_word_2_vec_lemma.npz")

    
    #parser = argparse.ArgumentParser(description="Create embedding in .npz file for NameTag model using an input .conll file used for training the BiLSMT model and pretrained embeddings")
    #parser.add_argument("embedding_file", help="Path to the embedding file")
    #parser.add_argument("input_file", help="Path to the input file")
    #parser.add_argument("output_file", help="Path to the output .npz file")
#
    #args = parser.parse_args()
    #process_input_file(args.embedding_file, args.input_file, args.output_file)

if __name__ == "__main__":
    main()
