def correct_labels(file_path: str):
    with open(file_path, 'r', encoding="utf-8") as f:
        lines = f.readlines()
        new_lines = []
        
        for line in lines:
            label = line.strip().split("\t")[-1]
            
            if label == 'B-REAL':
                label = 'B-REAL PERSON'
            elif label == 'I-REAL':
                label = 'I-REAL PERSON'
            elif label == 'B-MYSTIC':
                label = 'B-MYSTIC PERSON'
            elif label == 'I-MYSTIC':
                label = 'I-MYSTIC PERSON'
                
            new_lines.append("\t".join(line.strip().split("\t")[:-1]) + "\t" + label + "\n")
            
    with open(file_path, 'w', encoding="utf-8") as f:
        f.writelines(new_lines)
        
if __name__ == "__main__":
    correct_labels("./outputs/output_poems_v2_dev.txt")