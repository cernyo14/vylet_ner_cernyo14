import sys


def convert(filepath: str)-> str:
    '''
    Converts the .txt train file to .conll format    
    '''
    with open(filepath, 'r', encoding='utf-8') as f:
        lines = f.readlines()
    
    res = ""
    for line in lines:
        line_arr = line.strip().split("\t")
        if len(line_arr) < 2:
            res += "\n"
            continue
        
        ## add lemma and morph to one line
        res += line_arr[3] + "\t" + line_arr[2] + "\n"
        
        # add lemma and tag to one line
        # res += line_arr[3] + "\t" + line_arr[-1] + "\n"
        
        # add token and tag to one line
        # res += line_arr[1] + "\t" + line_arr[-1] + "\n"
        
        # add token and lemma and morph to one line
        # res += line_arr[3] + "\t" + line_arr[2] +  "\t" + line_arr[1] + "\n"
        
    return res

if __name__ == "__main__":  
    """
    Usage:
    python convert_text_to_conll.py input_file.txt output_file.conll
    """
    print("Converting to CoNLL format")  
    res = convert(sys.argv[1])
    print("Writing into output file")
    with open(sys.argv[2], 'w', encoding='utf-8') as f:
        f.write(res)
        
    
        
    
    