# Model implementation
Files that needed to be added to or edited from the [BiLSTM-CRF](https://github.com/UKPLab/emnlp2017-bilstm-cnn-crf) model to fit the needs for NER on Czech poetry

## Structure of this folder
- `data` folder with train, dev, test files
- `inputs` folder for input data for trained model
- `models` folder where trained models are saved into
- `outputs` folder for outputs created by running the model
- `pkl` folder with pickle files
- `test_results` folder contains `.txt` files of F1 scores of different model versions (different type of training data, embeddings or hyperparameters) 
- `CoNLL.py` and `preprocessing.py` are only slightly edited from the [original version](https://github.com/UKPLab/emnlp2017-bilstm-cnn-crf) of these files. These files needed to be edited in order to be able to read utf-8 characters.
- `BiLSTM.py` contains most of the neural net implementation. This file is also mostly similar to its original found in [here](https://github.com/UKPLab/emnlp2017-bilstm-cnn-crf). The only edit here is done in order for us to be able to use both pre-trained token and pre-trained lemma embedding (the model was created with usage of only one pre-trained embedding in mind).
- `RunModel_CoNLL_Format.py` loads pre-trained model and a input file in CoNLL format and runs this model (also based on file from [here](https://github.com/UKPLab/emnlp2017-bilstm-cnn-crf))
- `Train_NER.py` trains the neural network on data specified in this file (also based on file from [here](https://github.com/UKPLab/emnlp2017-bilstm-cnn-crf))
- `convert_text_to_conll.py` converts `.txt` files to `.conll` files so they can be used as inputs for running a trained model (in `RunModel_CoNLL_Format.py`)
- `correct_labels_conf_matrix.py` edits label format so they can be used in confusion matrix
- `join_person_entities.py` changes entities of Real Person into Person entities
- `requirements.txt`: Python packages installation file 
- `test_acc.py` tests models accuracy
- `word_2_vec_lemma.txt.gz` and `word_2_vec_token.txt.gz` are zipped files of trained word embeddings

## Environment to run the model
1. Create Python3 virtual environment: `python3 -m venv venv` or `py3 -m venv venv`
2. Activate virtual environment: `venv/bin/activate`
3. Install required packages: `pip3 install -r requirements.txt`