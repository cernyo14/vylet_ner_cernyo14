# This scripts loads a pretrained model and a input file in CoNLL format (each line a token, sentences separated by an empty line).
# The input sentences are passed to the model for tagging. Saves the output to a file.
# Usage: python RunModel_ConLL_Format.py modelPath inputPathToConllFile

import sys
import logging
import os
from preprocessing import readCoNLL, createMatrices, addCharInformation, addCasingInformation
from BiLSTM import BiLSTM


if len(sys.argv) < 3:
    print("Usage: python RunModel_CoNLL_Format.py modelPath inputPathToConllFile outputPath")
    exit()

modelPath = sys.argv[1]
inputPath = sys.argv[2]
outputPath = sys.argv[3]
# uncomment if token are used as input
# inputColumns = {0: "tokens", 1: "morph", 2: "real_tokens"}

# comment out if token are used as input
inputColumns = {0: "tokens", 1: "morph"}

# :: Prepare the input ::
sentences = readCoNLL(inputPath, inputColumns)
addCharInformation(sentences)
addCasingInformation(sentences)


# :: Load the model ::
lstmModel = BiLSTM.loadModel(modelPath)


dataMatrix = createMatrices(sentences, lstmModel.mappings, True)

# :: Tag the input ::
tags = lstmModel.tagSentences(dataMatrix)

# delete contents of output file (if exists)
if os.path.exists(outputPath):
    with open(outputPath, "w") as file:
        file.truncate(0)
    print(f"Contents of {outputPath} deleted since it was full before writing into it")
else:
    print(f"{outputPath} does not yet exist so it will be created")
    
'''# :: Output to stdout ::
for sentenceIdx in range(len(sentences)):
    tokens = sentences[sentenceIdx]['tokens']

    for tokenIdx in range(len(tokens)):
        tokenTags = []
        for modelName in sorted(tags.keys()):
            tokenTags.append(tags[modelName][sentenceIdx][tokenIdx])

        print("%s\t%s" % (tokens[tokenIdx], "\t".join(tokenTags)))
    print("")'''
    
# My Change: I added the following lines to save the output to a file
print("Saving to file")

'''outfilename = os.path.splitext(inputPath)[0] + ".txt"
outfilename = outfilename.replace("input", "output")
outfilepath = os.path.basename(f'./outputs/{outfilename}')

while os.path.exists(outfilepath):
    outfilepath = outfilepath.replace(".txt", "_1.txt")
    
print(outfilepath)'''

# Print to file in format: lemma tag

for sentenceIdx in range(len(sentences)):
    tokens = sentences[sentenceIdx]['tokens']
    res = ""
    
    for tokenIdx in range(len(tokens)):
        tokenTags = []
        for modelName in sorted(tags.keys()):
            tokenTags.append(tags[modelName][sentenceIdx][tokenIdx])
        
        res += ("%s\t%s\n" % (tokens[tokenIdx], "\t".join(tokenTags)))
    res += "\n" 
    with open(outputPath, 'a', encoding="utf-8") as f:
        f.write(res)


"""        
# Print to file in format: token lemma tag
for sentenceIdx in range(len(sentences)):
    tokens = sentences[sentenceIdx]['tokens']
    res = ""
    
    with open(inputPath, 'r', encoding="utf-8") as f:
        lines = f.readlines()
    
    for tokenIdx, line in zip(range(len(tokens)), lines):
        tokenTags = []
        for modelName in sorted(tags.keys()):
            tokenTags.append(tags[modelName][sentenceIdx][tokenIdx])
        
        if line.split():   
            token = line.split()[0]
        else:
            token = ""       
        res += ("%s\t%s\t%s\n" % (token, tokens[tokenIdx], "\t".join(tokenTags)))
    res += "\n" 
    with open(outputPath, 'a', encoding="utf-8") as f:
        f.write(res)
"""