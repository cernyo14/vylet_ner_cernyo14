from itertools import repeat
import multiprocessing  
import pickle
import os
import fnmatch
import shutil
import json
import book
import helper_func

def init_worker(val):
    global counter
    counter = val

def fill_category_worker(filename: str, ents: list, ents_types: list) -> list:
    '''
    Function for single process
    param ents: list of entities inside book/filename
    ents_types: list of unique entities across all books with filled category
    Match corresponding entities between ents and ents_type
    return: list of entities with filled category for this book/filename
    '''
    new_entities = []
    global counter
    for ent in ents:
        for ent_type in ents_types:
            if ent == ent_type:
                ent.category = ent_type.category
                new_entities.append(ent)

        
    #with counter.get_lock():
    #    counter.value += 1
    #
    #if counter.value % 100 == 0:
    #    print(counter.value)
    #            
    return filename, new_entities

def fill_category(book_ents_dict: dict, ents_type: list) -> dict:
    '''
    Multiprocessing function
    param book_ents_dict: dictionary of books and their entities (entities are not filled with category)
    ents_type: list of unique entities across all books with filled category
    Function fills category for each entity in book_ents_dict
    return: dictionary of books and their entities (entities are filled with category)
    '''
    val = multiprocessing.Value('i', 0)
    
    books = [filename for filename in book_ents_dict.keys()]
    entities = [ents_dict[filename] for filename in book_ents_dict.keys()]
    
    print("Starting multiprocessing")
    print("Number of processes: ", multiprocessing.cpu_count())
    print("Number of books filled (prints every 100th):")
    
    with multiprocessing.Pool(multiprocessing.cpu_count(), initializer=init_worker, initargs=(val, )) as p:
        book_ents_dict_type_list = p.starmap(fill_category_worker, zip(books, entities, repeat(ents_type)))
        
    with open("./src/pickles/book_ents_dict_type_list.plk", "wb") as f:
        pickle.dump(dict(book_ents_dict_type_list), f)
        
    return dict(book_ents_dict_type_list)
        
def clear_dir(dirpath: str) -> None:
    '''
    Clears contents of a directory (dirpath)
    '''
    for filename in os.listdir(dirpath):
        if filename == '.gitignore':
            continue
        file_path = os.path.join(dirpath, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))

def add_ents_to_bookfile_worker(input_path: str, dirpath: str, ents: list) -> None:
    '''
    Function for single process
    param input_path: path to book file
    dirpath: path to directory where book file will be saved
    ents: list of entities to add to book file
    Adds entities to book file and saves it to disk as json
    '''
    ents = helper_func.make_unique_multiword(ents)
    
    with open(input_path, "r") as input_file:
        book_json = json.load(input_file)
    
    curr_book = book.load_book(input_path)
    curr_book.add_ents(ents)
        
    for poem_json, poem in zip(book_json, curr_book.poems):
        for stanza_json, stanza in zip(poem_json["body"], poem.stanzas):
            for line_json, line in zip(stanza_json, stanza.lines):                
                for word_json, word in zip(line_json["words"], line.words):
                    word_json["entity"] = word.bio_ent
    
        
    filename = input_path.split('/')[-1]
    output_path = [filename]
    output_path.insert(0, dirpath)
    output_path = '/'.join(output_path)
    
    with open(output_path, "w") as output_file:
        json.dump(book_json, output_file, indent=4)
        
    #global counter
    #with counter.get_lock():
    #    counter.value += 1
    #if counter.value % 100 == 0:
    #    print(counter.value)


def add_ents_to_bookfiles(dirpath: str, book_ents_type: dict) -> None:
    '''
    Multiprocessing function
    param dirpath: path to directory where book files will be saved
    param book_ents_type: dictionary of books and their entities (entities are filled with category)
    Function adds entities to book files and saves them to disk as jsons
    '''    
    num_files = len(fnmatch.filter(os.listdir(dirpath), '*.*'))         
    if num_files != len(book_ents_type):
        clear_dir(dirpath)
    
    print("Starting multiprocessing")
    print("Number of processes: ", multiprocessing.cpu_count())
    print("Number of books processed (prints every 100th):")
    val = multiprocessing.Value('i', 0)
    
    with multiprocessing.Pool(multiprocessing.cpu_count(), initializer=init_worker, initargs=(val, )) as p:
        p.starmap(add_ents_to_bookfile_worker, zip(book_ents_type.keys(), repeat(dirpath), book_ents_type.values()))

def main(dirpath: str, book_ents_dict: dict, ents_type: list) -> None:
    '''
    Function fills entities per book with their corresponding category and 
    adds these entities to corresponding lines in books and saves these books to disk as jsons
    param dirpath: path to directory where book files will be saved
    param book_ents_dict: dictionary of books and their entities (entities are not filled with category)
    param ents_type: list of unique entities across all books with filled category
    '''
    
    print("Filling entities per book with their corresponding category")
    book_ents_type = fill_category(book_ents_dict, ents_type)
    print("All entities across all books filled with their corresponding category")
               
        
    print("Adding entities to corresponding lines in book and saving these books to disk as jsons")
    add_ents_to_bookfiles(dirpath, book_ents_type)
    print("All books processed and saved to disk")
    
if __name__ == '__main__':
    ents_dict = {}
    with open('./src/pickles/cs_potential_ents_uniq', 'rb') as f:
        ents_dict = pickle.load(f)

    ents_type = []
    with open('./src/pickles/cs_ents_types_all_wiki_only_no_other_less_all.plk', 'rb') as f:
        ents_type = pickle.load(f)
        
    bookname, ents = fill_category_worker("src/models_comparison/hand_tagged_test_files/cs_book_filled_ents.json", ents_dict, ents_type)
    for ent in ents:
        print(ent)
    add_ents_to_bookfile_worker("src/models_comparison/hand_tagged_test_files/cs_book_filled_ents.json", "src/models_comparison/hand_tagged_test_files", helper_func.make_unique_multiword(ents))
    #main('./book_ents_categorized_wiki_only_no_other_less_all', ents_dict, ents_type)

