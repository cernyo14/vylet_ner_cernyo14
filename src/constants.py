# --- To find entities in text ---
ALLOWED_MORPH = ['N', 'A', 'R', 'X', 'C']
NUMBER = 'C'
PREPOSITION = 'R'
ADJECTIVE = 'A'
NOUN = 'N'
UNKNOWN = 'X'
CASE_INDEX = 4
CASE_4 = 4

MAX_THREADS = 200

MAX_WORDS_ENT = 4
ALLOWED_POS = ['N', 'A', 'X']


# --- For entity categorization ---
ERROR_TYPE = "ERROR GETTING TYPE"
PLACE = "PLACE"
PERSON = "PERSON"
MYSTIC_PERSON = "MYSTIC PERSON"
ABSTRACT_ENTITY = "ABSTRACT ENTITY"
WARNING = "WARNING"
NOT_EXISTS = "NOT EXISTS"
REAL_PERSON = "REAL PERSON"
OTHER = "OTHER"
DISAMBIGUATION = "Only disambiguation page"
UNDEFINED = "O"
CHANCE_TYPE = "CHANCE TYPE"
SUSPICIOUS_TYPE = "SUSPICIOUS_TYPE"

VALID_TYPES = [PLACE, PERSON, MYSTIC_PERSON, ABSTRACT_ENTITY, REAL_PERSON, OTHER]

PLACE_CATEGORY = ["místopis", "míst", "stát", "země", "území", "geograf", "vojvodství", "kraj", "říše", 
                    "územ", "ostrov", "okres", "středisk"]
PERSON_CATEGORY = ["jmén", "národ", "lid", "král", "postav", "rod"]
MYSTIC_PERSON_CATEGORY = ["boh", "bůh", "bohyně", "mytologi"]



WIKIDATA_PLACE = ["village", "commune", "town", "city", "park", "settlement", "region", "country", "state", "river"]
WIKIDATA_PERSON = ["name", "character", "figure", "ethnic", "group", "researcher", "writer", "singer", 
                    "actor", "politician", "musician", "artist"]
WIKIDATA_MYSTIC_PERSON = ["mythological", "mythical", "god", "creature"]


# --- For visualization ---
PUNCT = "PUNCTUATION"
END_PUNCTS = [".", "!", "?", ":", "...", "„", "“", '\u2013', '-', ")"]

# --- For model comparison --
LINGUISTIC_MARKS = [
    '.', ',', '?', '!', ';', ':', '-', '--', "'", '"', '(', ')', '[', ']', '{', '}',
    '...', '/', '&', '%', '$', '#', '@', '+', '-', '=', '>', '<', '*', '^', '_',
    '~', '|', '`', '„', '“', '\u2013', '’', '““'
]
NUMBERS = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
