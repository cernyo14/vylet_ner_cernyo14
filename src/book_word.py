import constants

class Word:
    def __init__(self, token: str, morph: str, lemma: str, bio_ent: str, id: int) -> None:
        self.token, self.morph, self.lemma, self.bio_ent, self.id = token, morph, lemma, bio_ent, id
    
    def __str__(self) -> str:
        return self.token
    
    def is_capital_token(self) -> bool:
        return self.token[0].isupper()
    
    def is_capital_lemma(self) -> bool:
        return self.lemma[0].isupper()

    def is_noun(self) -> bool:
        return self.morph[0] == constants.NOUN
    
    def is_number(self) -> bool:
        return self.morph[0] == constants.NUMBER
    
    def is_preposition(self) -> bool:
        return self.morph[0] == constants.PREPOSITION
    
    def is_adjective(self) -> bool:
        return self.morph[0] == constants.ADJECTIVE
    
    def is_unknown(self) -> bool:
        return self.morph[0] == constants.UNKNOWN
    
    def get_POS(self) -> str:
        return self.morph[0]
    
    def get_case(self) -> str:
        return self.morph[constants.CASE_INDEX]
    
    def lenght_token(self) -> int:
        return len(self.token)
    
    def lenght_lemma(self) -> int:
        return len(self.token)

    def is_caps(self) -> bool:
        return self.token.isupper()
    
    def get_capital_lemma(self) -> str:
        "Returns the lemma with the first letter capitalized."
        return self.lemma.replace(self.lemma[0], self.lemma[0].capitalize(), 1)
    
    def is_ents_begin(self) -> bool:
        return self.bio_ent and self.bio_ent[0] == "B"
    
    def is_ents_inside(self) -> bool:
        return self.bio_ent and self.bio_ent[0] == "I"
