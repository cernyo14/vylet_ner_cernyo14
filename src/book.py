import constants
import book_word
import json
import os
from entity import Entity

class Line:
    def __init__(self, text: str, punct: dict, words: list) -> None:
        self.text = text
        self.punct = punct
        self.words = words
        self.ents = []
        
        for word in self.words:
            if word.is_ents_begin():
                self.ents.append(Entity([word], word.bio_ent[2:]))
                
            elif word.is_ents_inside():
                self.ents[-1].push(word)
                
                if word.bio_ent[2:] != self.ents[-1].category:
                    self.ents[-1].category = constants.NOT_EXISTS
        
    def __str__(self):        
        return self.text
    
    def update_ents(self) -> None:
        self.ents = []
        
        for word in self.words:
            if word.is_ents_begin():
                self.ents.append(Entity([word], word.bio_ent[2:]))
                
            elif word.is_ents_inside():
                self.ents[-1].push(word)
                
                if word.bio_ent[2:] != self.ents[-1].category:
                    self.ents[-1].category = constants.NOT_EXISTS
        
    def get_lemma_text(self) -> str:
        '''
        Returns line of text with lemmas of words and punctuation
        '''
        lemma_line = ""
    
        for index, word in enumerate(self.words):
            if str(index + 1) in self.punct:
                lemma_line += f'{word.lemma}'
                lemma_line += f'{self.punct[str(index + 1)]} '
            else:
                lemma_line += f'{word.lemma} '

        return lemma_line
    
    def get_lemma_text_no_punct(self) -> str:
        '''
        Returns line of text with lemmas of words and NO punctuation
        '''
        lemma_line = ""
    
        for word in self.words:
            lemma_line += f'{word.lemma} '

        return lemma_line
    
    def get_word_index_by_id(self, id: int) -> int:
        '''
        Returns index of word in line by id
        '''
        for index, word in enumerate(self.words):
            if word.id == id:
                return index
        
        return -1
    
            
    def get_words_visualization(self) -> list:
        #TODO - redo
        res = []
        
        for index, word in enumerate(self.words):
            append_tuple = (word.token, "")
            
            for ent_pos in self.ents_pos:
                if ent_pos.is_on_position(index):
                    append_tuple = (word.token, ent_pos.entity.category)
                    break
                
            res.append(append_tuple)
            
            if str(index + 1) in self.punct:
                res.append((self.punct[str(index + 1)], constants.PUNCT))
        
        return res
    
    def get_words_for_model(self) -> list:
        """
        Gets words for model
        @return: list of tuples (word, category done in BIO tagging style)
        """
        res = []
        
        for index, word in enumerate(self.words):
            append_tuple = (word, "O")
            
            for ent_pos in self.ents_pos:
                if ent_pos.is_on_position(index):
                    if len(ent_pos.entity) > 1 and ent_pos.positions[0] < index:
                        append_tuple = (word, f'I-{ent_pos.entity.category}')
                        break
                    
                    append_tuple = (word, f'B-{ent_pos.entity.category}')
                    break
                
            res.append(append_tuple)
            
            if str(index + 1) in self.punct:
                punct_word = book_word.Word(self.punct[str(index + 1)], "--------------", self.punct[str(index + 1)], 0)
                res.append((punct_word, "O"))
        
        return res
    
    def get_text(self) -> list:
        res = []
        
        for index, word in enumerate(self.words):
            if str(index + 1) in self.punct:
                res.append(word.token)
                res.append(f'{self.punct[str(index + 1)]} ')
            else:
                res.append(f'{word.token} ')

        return res
    
    def same_capital_sequence_words(self, words1: list, words2: list) -> bool:
        if len(words1) != len(words2):
            return False
        
        for word1, word2 in zip(words1, words2):
            if word1.lemma != word2.lemma:
                return False
            
            if not word1.is_capital_token() or not word2.is_capital_token():
                return False
            
        return True
    
    def contains_ent_lemma(self, ent: Entity) -> bool:
        
        ent_lemma_form = ent.to_str_lemma()
        
        if not ent_lemma_form in self.get_lemma_text():
            return False
        
        for i in range(len(self.words) - len(ent.words) + 1):
            if self.same_capital_sequence_words(self.words[i:i + len(ent.words)], ent.words):
                return True   
        
        return False
        
            
class Stanza:
    def __init__(self, lines: list) -> None:
        self.lines = lines
        
    def __str__(self):
        res = ''
        for line in self.lines:
            res += str(line) + '\n'
        
        return res
        
    def get_lemma_text(self) -> str:
        '''
        Returns stanza of text with lemmas of words and punctuation
        '''
        lemma_stanza = ""
        
        for line in self.lines:
            lemma_stanza += f'{line.get_lemma_text_no_punct()}'

        return lemma_stanza
    
    def get_ents(self) -> list:
        '''
        Returns list of entities in stanza
        '''
        return [ent for line in self.lines for ent in line.ents]

    
    def get_words(self) -> list:
        '''
        Returns list of words in stanza
        '''
        return [word for line in self.lines for word in line.words]
        
    def get_words_token(self) -> list:
        '''
        Returns list of word in stanza
        '''
        res = []
        
        for line in self.lines:
            for word in line.words:
                res.append(word.token)
            
        return res
        
    def get_words_lemma(self) -> list:
        '''
        Returns list of word in stanza
        '''
        res = []
        
        for line in self.lines:
            for word in line.words:
                res.append(word.lemma)
            
        return res
    
    def save_training_data_model(self, filename: str):
        '''
        Saves stanza to file so it can be used as training data in BiLSTM model
        '''
        res = "" 
        
        for pos, word in enumerate([word for line in self.lines for word in line.words], 1):
            res += f"{pos}\t{word.token}\t{word.morph}\t{word.lemma}\t{word.bio_ent}\n"
            
        res += "\n"
        
        with open(filename, 'w', encoding='utf-8') as f:
            f.write(res)

class Poem:
    def __init__(self, stanzas: list) -> None:
        self.stanzas = stanzas
        
    def __str__(self):
        res = ''
        for stanza in self.stanzas:
            res += str(stanza) + '\n'
        
        return res
        
    def get_ents(self) -> list:
        '''
        Returns list of entities in poem
        '''
        return [stanza.get_ents() for stanza in self.stanzas]
    
    def get_valid_ents(self) -> list: 
        '''
        Returns list of valid entities in poem
        '''
        return [ent for stanza in self.stanzas for ent in stanza.get_ents() if ent.is_valid_type()]
    
    def get_lines(self) -> list:
        return [line for stanza in self.stanzas for line in stanza.lines]
    
    def save_to_file_text(self, filename: str) -> None:
        '''
        Saves poem to file
        '''
        with open(filename, 'w', encoding='utf-8') as f:
            for line in self.get_lines():
                f.write(f'{line.text}\n')
    
    def save_training_data_model(self, filename: str):
        '''
        Saves poem to file so it can be used as training data in BiLSTM model
        '''
        res = ""
        for stanza in self.stanzas:
            words_cat = []
            for line in stanza.lines:
                words_cat += line.get_words_for_model()
        
            pos = 1
            for word, category in words_cat:
                res += f"{pos}\t{word.token}\t{word.morph}\t{word.lemma}\t{category}\n"
                pos += 1
            res += "\n"
        

        with open(filename, 'w', encoding='utf-8') as f:
            f.write(res)


class Book:
    def __init__(self, filename: str, poems: list, ents_num = 0) -> None:
        self.poems = poems
        self.filename = filename
        self.num_ents_no_unique = ents_num
        
    def __str__(self):
        res = ''
        for poem in self.poems:
            res += str(poem) + '\n'
        
        return res
        
    def get_lines(self) -> list:
        res = []
        
        for poem in self.poems:
            for stanza in poem.stanzas:
                for line in stanza.lines:
                    res.append(line)
        
        return res
    
    def get_words(self) -> list:
        res = []
        
        for line in self.get_lines():
            for word in line.words:
                res.append(word)
        
        return res
    
    def num_of_words(self) -> int:
        return len(self.get_words())
    
    def get_unknown_words(self) -> list:
        res = []
        
        for word in self.get_words():
            if word.is_unknown():
                res.append(word)
        
        return res
    
    def num_of_unknown_words(self) -> int:
        return len(self.get_unknown_words())

    def find_lines_token(self, ent: Entity) -> list:
        res = []
        
        ent_token = ent.to_str_token()
        
        for line in self.get_lines():
            if ent_token in line.text:
                res.append(line.text)
        
        return res
    
    def find_lines_lemma(self, ent: Entity) -> list:
        res = []
        
        ent_lemma_form = ent.to_str_lemma()
        
        for line in self.get_lines():
            if ent_lemma_form in line.get_lemma_text():
                ent_fail = False
                for word_ent in ent.words:
                    for word_line in line.words:
                        if word_ent.lemma == word_line.lemma:
                            if not word_line.is_capital_token():
                                ent_fail = True
                                break
                if not ent_fail:                
                    res.append(line.text)
        return res
    
    def get_stanzas(self) -> list:
        res = []
        
        for poem in self.poems:
            for stanza in poem.stanzas:
                res.append(stanza)
        
        return res
                    
    def add_ents(self, ents: list) -> None:
        """
        Adds entities to lines so we know on which positions in line these entities are
        """
        for line in self.get_lines():
            for ent in ents:
                if not ent.to_str_lemma() in line.get_lemma_text():
                    continue
                
                line_words_iter = iter(line.words)
                for index, line_word in enumerate(line_words_iter):
                    if not line_word.is_capital_token():
                        continue
                                                
                    if ent.words[0].lemma == line_word.lemma:
                        for i, ent_word in enumerate(ent.words):                            
                            prefix = "B-" if i == 0 else "I-"
                            
                            if line.words[index].bio_ent == "O":
                                line.words[index].bio_ent = prefix + ent.category
                            
                            if i == (len(ent.words) - 1):
                                break
                            try:
                                next(line_words_iter)
                                index += 1
                            except StopIteration:
                                pass

            
    def num_of_stanzas(self) -> int:
        return len(self.get_stanzas())
    
    def stanzas_with_valid_entities(self) -> list:
        res = []
        
        for stanza in self.get_stanzas():
            if len(stanza.get_ents()) > 0:
                res.append(stanza)
        
        return res
    
    def get_ents(self) -> list:
        res = []
        
        for stanza in self.get_stanzas():
            res += stanza.get_ents()
        
        return res
    
    def get_starting_words(self) -> list:
        '''
        Get all words that start a sentence
        '''
        starting_words = []

        for poem in self.poems:
            new_line = True
            for stanza in poem.stanzas:
                new_line = True
                for line in stanza.lines:
                    # append word if we ended with a ending character prev line
                    if new_line and line.words[0].is_capital_token():
                        starting_words.append(line.words[0])
                        new_line = False
                    
                    # append word if we ended with a ending character before it
                    for index, word in enumerate(line.words):
                        index = str(index)
                        if index not in list(line.punct.keys()):
                            continue
                        elif line.punct[index][-1] in constants.END_PUNCTS and word.is_capital_token():
                            starting_words.append(word)
                    
                    # set a newline to True when we ended line with a ending character
                    if str(len((line.words))) in list(line.punct.keys()) and line.punct[str(len(line.words))][-1] in constants.END_PUNCTS:
                        new_line = True

        return starting_words

    def get_capital_words(self) -> list:
        """
        Returns list of all words that start with capital letter in the book
        @returns: list of strings
        """
        return [word for line in self.get_lines() for word in line.words if word.is_capital_token()]
    
    def get_capital_lemmas(self) -> list:
        """
        Returns list of all words that have capital lemma
        @returns: list of book_word.Word
        """
        return [word for line in self.get_lines() for word in line.words if word.is_capital_lemma()]
    
    def allowed_cap_POS_words(self) -> list:
        """
        Returns list of all words that have allowed POS and have capital first word
        @returns: list of book_word.Word
        """
        return [word for word in self.get_capital_words() if word.get_POS() in constants.ALLOWED_POS]
        
    def get_potential_ents(self) -> list:
        '''
        Get all potential entities in the book
        Helper func to get all potential ents
        @returns: list of Entity
        '''
        multi_ents_begin = []
        
        for line in self.get_lines():
            ent = Entity([])
            for index, word in enumerate(line.words):
                if index == 0:
                    continue
                # if linguistic mark at this index append current entities sequences to multi_ents_begin
                if str(index) in list(line.punct.keys()) and line.punct[str(index)][0] in constants.LINGUISTIC_MARKS:
                    multi_ents_begin.append(ent) if len(ent.words) > 0 else None
                    ent = Entity([])

                # if current word is capital and has allowed POS add the word to entities sequences if they have the same case   
                # or start a new sequence of words if they dont have a same case                          
                if word.is_capital_token() and word.get_POS() in constants.ALLOWED_POS:
                        ent.words.append(word)
                # elif word is number then if it is in capital and in the same case as prev 
                elif word.is_capital_token() and word.is_number() and ent.words:
                    if all(curr_word.get_case() == word.get_case() for curr_word in ent.words):
                        ent.words.append(word)
                # else (current word is not capital, ...) append current word sequence to multi_ents_begin
                else:
                    multi_ents_begin.append(ent) if len(ent.words) > 0 else None
                    ent = Entity([])
            
            multi_ents_begin.append(ent) if len(ent.words) > 0 else None

        return multi_ents_begin
    
    def check_cases(self, ents: list) -> list:
        '''
        Checks if all all words in one Entity are in the same case.
        @returns: list of Entity
        '''
        checked_ents = []

        for ent in ents:

            curr_case = ent.words[0].get_case()
            start = 0

            for index, word in enumerate(ent.words):
                if word.get_case() != curr_case:
                    checked_ents.append(Entity(ent.words[start:index]))
                    start = index
                    curr_case = word.get_case()
            checked_ents.append(Entity(ent.words[start:len(ent.words)]))
        
        return checked_ents
    
    def get_all_potential_entities(self) -> list:
        '''
        Get all potential entities in the book
        @returns: list of Entity
        '''
        all_ents = self.get_potential_ents()
        # remove entities that contain morph unknown words
        all_ents = [ent for ent in all_ents if not ent.contains_unknown()]
        # check cases of mw entities
        all_ents = self.check_cases(all_ents)
        # remove entities that are only at a beginning of a sentence
        return [ent for ent in all_ents if ent.words[0] not in self.get_starting_words()]
    
    def get_sentences(self) -> list:
        '''
        Get all sentences in the book
        @returns: list of lists of tuples (word, bio_label)
        
        '''
        sentences = []
        starting_words = self.get_starting_words()
        
        for stanza in self.get_stanzas():
            sentence = []            
            for word in stanza.get_words():
                if word in starting_words:
                    sentences.append(sentence) if sentence else None
                    sentence = []
                sentence.append((word.token, word.bio_ent))
                
        return sentences
    
    def get_sentences_new(self) -> list:
        '''
        Get all sentences in the book
        @returns: list of lists of tuples (word, bio_label)        
        '''
        #def four_lines_chunks(stanza) -> list:
        #    stanza_sentences = []
        #    sentence = []
        #    
        #    for index, line in enumerate(stanza.lines):
        #        if index+1 % 4 == 0:
        #            stanza_sentences.append(sentence)
        #        sentence.extend([(word.token, word.bio_ent) for word in line.words])
        #    
        #    stanza_sentences.extend([(word.token, word.bio_ent) for word in line.words])
        #    
        #    return stanza_sentences
        #
        
        sentences = []
        starting_words = self.get_starting_words()
        
        for stanza in self.get_stanzas():
            if len(stanza.lines) < 6:
                sentences.append([(word.token, word.bio_ent) for word in stanza.get_words()])
                continue

            sentence = []            
            for word in stanza.get_words():
                if word in starting_words:
                    sentences.append(sentence) if sentence else None
                    sentence = []
                    
                if len(sentence) > 50:
                    sentences.append(sentence) if sentence else None
                    sentence = []
                    
                sentence.append((word.token, word.bio_ent))
                
            sentences.append(sentence) if sentence else None
                    
        return sentences    
    
    def book_to_CoNLL(self, output_path: str) -> None:
        """
        Saves book in CoNLL format
        """
        with open(output_path, "w", encoding="utf-8") as f:
            for poem in self.poems:
                for stanza in poem.stanzas:
                    for line in stanza.lines:
                        for index, word in enumerate(line.words):
                            if str(index) in line.punct:
                                f.write(f'{line.punct[str(index)]}\t--------------\n')
                            f.write(f'{word.lemma}\t{word.morph}\n')
                            
                        if str(len(line.words)) in line.punct:
                                f.write(f'{line.punct[str(len(line.words))]}\t--------------\n')
                            
                    f.write('\n')


def load_book(book_path: str) -> Book:
    '''
    Loads book from json file and saves it into custom Book class
    In this case it also adds entities to lines but no ids
    '''
    with open(book_path) as json_file:
        data = json.load(json_file)
    
    num_ents_no_unique = 0
    poems = []
    for poem in data:
        stanzas = []
        for stanza in poem["body"]:
            lines = []
            
            for line in stanza:
                new_words = [book_word.Word(word["token"], word["morph"], word["lemma"], word.get("entity", "O"), word.get("id", -1)) for word in line["words"]]
                    
                lines.append(Line(line["text"], line["punct"], new_words))
            
            stanzas.append(Stanza(lines))
        poems.append(Poem(stanzas))
    
    return Book(book_path, poems, num_ents_no_unique)



def convert_books_to_CoNLL(inDirPath: str, outDirPath: str) -> None:
    '''
    Converts books in dirPath from json format to CoNLL format and saves them to output_dir
    '''
    cnt = 0
    for book_json in os.listdir(inDirPath):
        cnt += 1
        if cnt % 200 == 0:
            print(str(cnt) + " books converted")
        book = load_book(os.path.join(inDirPath, book_json))
        book.book_to_CoNLL(os.path.join(outDirPath, book_json[:-5] + ".conll"))
    
    print("All books converted")
    
    
#curr_book = load_book("src/models_comparison/hand_tagged_test_files/cs_book.json")
#words = curr_book.get_starting_words()
#i = 0
#for word in words:
#    i += 1
#    print(word)
#    if i == 10:
#        break

