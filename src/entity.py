from book_word import Word
import copy
import constants

class Entity:
    def __init__(self, words: list = [], category = constants.UNDEFINED, wiki_link = "") -> None:
        self.words = words
        self.category = category
        self.wiki_link = wiki_link
        
    def __eq__(self, other):
        return self.get_lemmas() == other.get_lemmas()
        
    def __len__(self) -> int:
        return len(self.words)
    
    def __str__(self) -> str:
        return self.to_str_lemma()
    
    def __hash__(self):
        return hash(str(self))
        
    def push(self, word: Word) -> None:
        self.words.append(copy.deepcopy(word))

    def set_type(self, category: str) -> None:
        self.category = category
    
    def get_lemmas(self) -> list:
        res = []
        for word in self.words:
            res.append(word.lemma)
        return res
    
    def get_tokens(self) -> list:
        res = []
        for word in self.words:
            res.append(word.token)
        return res
    
    def is_empty(self) -> bool:
        return bool(self.words) 
    
    def is_sw_ent(self) -> bool:
        return len(self) < 2
    
    def is_multi_noun(self) -> bool:
        cnt = 0
        for word in self.words:
            if word.is_noun():
                cnt += 1
        return cnt > 1
    
    def contains_unknown(self) -> bool:
        for word in self.words:
            if word.is_unknown():
                return True
        return False
    
    def to_str_token(self) -> str:
        return " ".join(self.get_tokens())

    def to_str_lemma(self) -> str:
        return " ".join(self.get_lemmas())
    
    def to_str_lemma_capital(self) -> str:
        '''
        Returns the lemmas of the entity with the first letter capitalized.
        They will be capitalized only if the token is capital.
        '''
        res = []
        for word in self.words:
            if word.is_capital_token() or word.is_capital_lemma():
                res.append(word.get_capital_lemma())
            else:
                res.append(word.lemma)
        return " ".join(res)
    
    def clear(self) -> None:
        self.words = []
    
    def get_nouns(self) -> list:
        res = []
        for word in self.words:
            if word.is_noun():
                res.append(word)
        return res
    
    def contains_id(self, id: int) -> bool:
        for word in self.words:
            if word.id == id:
                return True
        return False
    
    def contains_lemma(self, lemma: str) -> bool:
        for word in self.words:
            if word.lemma == lemma:
                return True
        return False
    
    def is_valid_type(self) -> bool:
        return True if self.category in constants.VALID_TYPES else False
    
    def to_json(self) -> dict:
        res = {}
        res["token_form"] = self.to_str_token()
        res["lemma_form"] = self.to_str_lemma()
        res["category"] = self.category
        res["wiki_url"] = self.wiki_url
        
        
        return res
    
    
#tmp = Entity([Word("Duch", "----", "duch", 5), Word("Svatý", "----", "svatý", 5)])
#print(tmp.is_valid_type())