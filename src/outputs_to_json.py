import os
import constants

def book_add_ents_to_json(book_path, model_output_path):
    """
    Adds the entities from the model output to the json file
    """
    import json
    import os

    if not os.path.isfile(book_path):
        raise FileNotFoundError("Book not found.")
    
    if not os.path.isfile(model_output_path):
        raise FileNotFoundError("Model output not found.")
    
    book_filename = os.path.basename(book_path)
    
    with open(book_path) as json_file:
        data = json.load(json_file)
        
    with open(model_output_path, 'r', encoding='utf8') as file:
        lines_output = file.readlines()
        
    for poem in data:
        for stanza in poem["body"]:            
            for line in stanza:
                for index, word in enumerate(line["words"]):
                    token_category = lines_output.pop(0)
                    while token_category.split("\t")[0].strip() != word["lemma"]:
                        token_category = lines_output.pop(0)
                    line["words"][index]["entity"] = token_category.split("\t")[-1].strip()
    
    with open("./resulting_books/" + book_filename, 'w') as f:
        json.dump(data, f, indent=4)
        
    
for file in os.listdir("../resources/corpusCzechVerse/ccv"):
    book_add_ents_to_json("../resources/corpusCzechVerse/ccv/" + file, "./model_implementation/outputs/all_books/" + file.split('.')[0] + ".txt")
    
    