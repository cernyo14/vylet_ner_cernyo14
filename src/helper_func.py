import os
import random
import threading
import constants
from book import load_book, Book
from entity import Entity

def words_to_ents(words: list) -> list:
    return [Entity([word]) for word in words]

def make_unique_multiword(entities: list) -> list:
    """
    Makes the list of entities given in parameter unique
    @param entities: list of entities
    @return: list of unique entities
    """
    unique_list = []
    unique_strings = set()
    
    for ent in entities:     
        multiword = ent.to_str_lemma()
        
        if multiword not in unique_strings:
            unique_strings.add(multiword)
            unique_list.append(ent)
    
    return unique_list


dictLock = threading.Lock()


def load_and_run(res, filename: str, func):
    '''
    loads book and runs a given function in param on it
    Worker function for a single thread
    Saves ouput of given function into dict
    '''
    book = load_book(f'{filename}')
    
    return_val = func(book)
    dictLock.acquire()
    res[filename] = return_val
    dictLock.release()

def run_multiple_books_multithreaded(func, num = len(os.listdir("../books_word_ids/"))) -> dict:
    '''
    Runs one function on multiple books in parallel
    @return: dict {filename of book: output of given func}
    '''
    dir_path = "./books_word_ids/"
    
    if num < 1 or num > len(os.listdir(dir_path)):
        raise Exception("Number of books out of bounds")

    files_pos = random.sample(range(0, len(os.listdir(dir_path))), num)

    files = []
    for i, filename in enumerate(os.listdir(dir_path)):
        if i in files_pos:
            if filename == ".gitignore":
                continue
            files.append(dir_path + filename)
            
    res_dict = dict()
    while files:
        threads = []
        for i in range(constants.MAX_THREADS):
            if not files:
                break
            
            filename = files.pop(0)
            thread = threading.Thread(target=load_and_run, args=(res_dict, filename, func))
            thread.start()
            threads.append(thread)

        for thread in threads:
            thread.join()
        
    return res_dict


def save_ents_multiple_books(ents_books: dict, filepath: str):
    save_file = open(filepath, "w+", encoding='utf-8')

    for filename, ents in ents_books.items():
        save_file.write(f"Book file: {filename}\n")
        book = load_book(f"{filename}")

        for ent in make_unique_multiword(ents):
            ent_lines = book.find_lines_lemma(ent)
            for word in ent.words:
                save_file.write(f'{word.token}/{word.lemma} ({word.morph}) ')
            save_file.write(f' | {ent_lines}\n')

        save_file.write('\n')

    save_file.close()


def train_dev_test_split(dirpath_empty: str, dirpath_with_ents: str, dirpath_empty_txt: str, dirpath_with_ents_txt: str, destDirectory: str, test_size = 0.15, dev_size = 0.15) -> None:
    # runtime: 100min
    '''
    Creates Train, Dev and Test dataset and saves them
    @param dirpath_empty: path to directory that contains poems which dont have any entities
    @param dirpath_with_ents: path to directory that contains poems that have at least one entity in them
    @param destDirectory: path to directory into which the train, dev, test split should be saved
    @param test_size: how much (0-1) of the data should be used for test dataset
    @param dev_size: how much (0-1) of the data should be used for dev dataset out of the data remaining after subtracting test dataset
    '''

    dir_empty_len = len(os.listdir(dirpath_empty))
    dir_ents_len = len(os.listdir(dirpath_with_ents))

    test_volume_ents = dir_ents_len * test_size
    dev_volume_ents = (dir_ents_len - test_volume_ents) * dev_size
    train_volume_ents = dir_ents_len - test_volume_ents - dev_volume_ents

    # We want the same amount of empty to full stanzas
    test_volume_empty = dir_ents_len * test_size
    dev_volume_empty = (dir_ents_len - test_volume_empty) * dev_size
    train_volume_empty = dir_ents_len - test_volume_empty - dev_volume_empty
    
    random.seed(333)


    # write test files
    print("Creating test data")
    test_files_ents = random.sample(os.listdir(dirpath_with_ents), int(test_volume_ents))
    test_files_empty = random.sample(os.listdir(dirpath_empty), int(test_volume_empty))
    res = ""
    res_txt = ""
    for fname in test_files_ents:
        with open(os.path.join(dirpath_with_ents, fname), 'r', encoding="utf-8") as f:
            res += f.read()
        with open(os.path.join(dirpath_with_ents_txt, fname), 'r', encoding="utf-8") as f:
            res_txt += f.read()
            
    for fname in test_files_empty:
        with open(os.path.join(dirpath_empty, fname), 'r', encoding="utf-8") as f:
            res += f.read()
        with open(os.path.join(dirpath_empty_txt, fname), 'r', encoding="utf-8") as f:
            res_txt += f.read()

    with open(f'{destDirectory}/test.txt', 'w', encoding='utf-8') as f:
        f.write(res)
    with open(f'{destDirectory}/test_txt.txt', 'w', encoding='utf-8') as f:
        f.write(res_txt)


    # write dev files      
    print("Creating dev data")
    dev_files_ents = random.sample([f for f in os.listdir(dirpath_with_ents) if f not in test_files_ents], int(dev_volume_ents))
    print("dev data ents done")
    dev_files_empty = random.sample([f for f in os.listdir(dirpath_empty) if f not in test_files_empty], int(dev_volume_empty))
    print("dev data empty done")
    res = ""
    res_txt = ""
    for fname in dev_files_ents:
        with open(os.path.join(dirpath_with_ents, fname), 'r', encoding="utf-8") as f:
            res += f.read()
        with open(os.path.join(dirpath_with_ents_txt, fname), 'r', encoding="utf-8") as f:
            res_txt += f.read()

    for fname in dev_files_empty:
        with open(os.path.join(dirpath_empty, fname), 'r', encoding="utf-8") as f:
            res += f.read()
        with open(os.path.join(dirpath_empty_txt, fname), 'r', encoding="utf-8") as f:
            res_txt += f.read()
            
    with open(f'{destDirectory}/dev.txt', 'w', encoding='utf-8') as f:
        f.write(res)
    with open(f'{destDirectory}/dev_txt.txt', 'w', encoding='utf-8') as f:
        f.write(res_txt)


    # write train files
    print("Creating train data")
    train_files_ents = [f for f in os.listdir(dirpath_with_ents) if f not in test_files_ents and f not in dev_files_ents]
    print("train files ents done")
    train_files_empty = random.sample([f for f in os.listdir(dirpath_empty) if f not in test_files_empty and f not in dev_files_empty], int(train_volume_ents))
    #train_files_empty = [f for f in os.listdir(dirpath_empty) if f not in test_files_empty and f not in dev_files_empty]
    print("train files empty done")
    res = ""
    res_txt = ""
    for fname in train_files_ents:
        with open(os.path.join(dirpath_with_ents, fname), 'r', encoding="utf-8") as f:
            res += f.read()
        with open(os.path.join(dirpath_with_ents_txt, fname), 'r', encoding="utf-8") as f:
            res_txt += f.read()

    print("train files ents joined")

    for fname in train_files_empty:
        with open(os.path.join(dirpath_empty, fname), 'r', encoding="utf-8") as f:
            res += f.read()
        with open(os.path.join(dirpath_empty_txt, fname), 'r', encoding="utf-8") as f:
            res_txt += f.read()

    with open(f'{destDirectory}/train.txt', 'w', encoding='utf-8') as f:
        f.write(res)
    with open(f'{destDirectory}/train_txt.txt', 'w', encoding='utf-8') as f:
        f.write(res_txt)

    print("Train data has been created and written")
    

def train_dev_test_split_smaller(dirpath_empty: str, dirpath_with_ents: str, destDirectory: str, test_size = 0.15, dev_size = 0.15, size = 0.2) -> None:
    # runtime: 5min
    '''
    Creates Train, Dev and Test dataset and saves them
    All of these 3 dataset
    @param dirpath_empty: path to directory that contains poems which dont have any entities
    @param dirpath_with_ents: path to directory that contains poems that have at least one entity in them
    @param destDirectory: path to directory into which the train, dev, test split should be saved
    @param test_size: how much (0-1) of the data should be used for test dataset
    @param dev_size: how much (0-1) of the data should be used for dev dataset out of the data remaining after subtracting test dataset
    @param size: how much (0-1) from the original poems should be used
    '''

    dir_empty_len = len(os.listdir(dirpath_empty)) * size
    dir_ents_len = len(os.listdir(dirpath_with_ents)) * size

    test_volume_ents = dir_ents_len * test_size
    dev_volume_ents = (dir_ents_len - test_volume_ents) * dev_size
    train_volume_ents = dir_ents_len - test_volume_ents - dev_volume_ents

    test_volume_empty = dir_empty_len * test_size
    dev_volume_empty = (dir_empty_len - test_volume_empty) * dev_size
    train_volume_empty = dir_empty_len - test_volume_empty - dev_volume_empty

    # write test files
    print("Creating test data")
    test_files_ents = random.sample(os.listdir(dirpath_with_ents), int(test_volume_ents))
    test_files_empty = random.sample(os.listdir(dirpath_empty), int(test_volume_empty))
    res = ""
    for fname in test_files_ents:
        with open(os.path.join(dirpath_with_ents, fname), 'r', encoding="utf-8") as f:
            res += f.read()

    for fname in test_files_empty:
        with open(os.path.join(dirpath_empty, fname), 'r', encoding="utf-8") as f:
            res += f.read()

    with open(f'{destDirectory}/test.txt', 'w', encoding='utf-8') as f:
        f.write(res)


    # write dev files      
    print("Creating dev data")
    dev_files_ents = random.sample([f for f in os.listdir(dirpath_with_ents) if f not in test_files_ents], int(dev_volume_ents))
    print("dev data ents done")
    dev_files_empty = random.sample([f for f in os.listdir(dirpath_empty) if f not in test_files_empty], int(dev_volume_empty))
    print("dev data empty done")
    res = ""
    for fname in dev_files_ents:
        with open(os.path.join(dirpath_with_ents, fname), 'r', encoding="utf-8") as f:
            res += f.read()

    for fname in dev_files_empty:
        with open(os.path.join(dirpath_empty, fname), 'r', encoding="utf-8") as f:
            res += f.read()

    with open(f'{destDirectory}/dev.txt', 'w', encoding='utf-8') as f:
        f.write(res)


    # write train files
    print("Creating train data")
    train_files_ents = random.sample([f for f in os.listdir(dirpath_with_ents) if f not in test_files_ents and f not in dev_files_ents], int(train_volume_ents))
    print("train files ents done")
    train_files_empty = random.sample([f for f in os.listdir(dirpath_empty) if f not in test_files_empty and f not in dev_files_empty], int(train_volume_empty))
    print("train files empty done")
    res = ""
    for fname in train_files_ents:
        with open(os.path.join(dirpath_with_ents, fname), 'r', encoding="utf-8") as f:
            res += f.read()

    print("train files ents joined")

    for fname in train_files_empty:
        with open(os.path.join(dirpath_empty, fname), 'r', encoding="utf-8") as f:
            res += f.read()

    print("train files empty joined")

    with open(f'{destDirectory}/train.txt', 'w', encoding='utf-8') as f:
        f.write(res)

    print("Train data has been created and written")
    

import pickle

#with open("./src/pickles/potential_ents", 'wb') as file:
#    pickle.dump(run_multiple_books_multithreaded(Book.get_all_potential_entities), file)
    
#ents = []
#for key, value in run_multiple_books_multithreaded(Book.get_all_potential_entities).items():
#    ents.extend(value)
#with open("./src/pickles/potential_ents_uniq", 'wb') as file:
#    pickle.dump(make_unique_multiword(ents), file)
#    

#save_ents_multiple_books(run_multiple_books_multithreaded(Book.get_all_potential_entities), "./src/txt_files/all_ents_lines.txt")


#with open("./src/pickles/potential_ents", 'rb') as file:
#    tmp_dict = pickle.load(file)
#    
#with open("./src/pickles/valid_ents_uniq.plk", 'rb') as file:
#    valid_ents = pickle.load(file)
#    
#with open("./src/pickles/valid_ents_wiki_only_uniq.plk", 'rb') as file:
#    valid_ents_wiki_only = pickle.load(file)
#    
#for key in tmp_dict:
#    tmp_dict[key] = valid_ents
#
#save_ents_multiple_books(tmp_dict, "./src/txt_files/all_ents_valid_ents.txt")
#
#for key in tmp_dict:
#    tmp_dict[key] = valid_ents_wiki_only
#    
#save_ents_multiple_books(tmp_dict, "./src/txt_files/all_ents_valid_ents_wiki_only.txt")

#train_dev_test_split("./training_stanzas_wiki_only/empty", "./training_stanzas_wiki_only/ents", "./training_stanzas_wiki_only_txt/empty", "./training_stanzas_wiki_only_txt/ents", "./model_implementation/data/Stanzas_newer_version")