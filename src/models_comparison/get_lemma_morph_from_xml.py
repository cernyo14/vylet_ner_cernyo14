import re
from NameTag_to_BIO import convert_to_xml
import os
import html
import sys
script_dir = os.path.dirname(os.path.abspath(__file__))
module_dir = os.path.join(script_dir, '..')
sys.path.append(module_dir)
import constants


def clean_lemmas(lemma: str) -> str:
    '''
    Cleans lemmas so all character after marks are deleted (the mark included)
    '''
    has_letter = False
    for char in lemma:
        if char.isalpha():
            has_letter = True
            continue
        
        if has_letter and char in constants.LINGUISTIC_MARKS:
            return lemma[:lemma.index(char)]
    
    return lemma
    

import re
import html

def extract_lemmas_tags_and_text(input_file_path, output_file_path):
    # Regular expression pattern to extract lemma, tag, and text from a token
    token_pattern = r'<token lemma="(.*?)" tag="(.*?)">(.*?)</token>'
    
    # Open input and output files
    with open(input_file_path, 'r', encoding='utf-8') as input_file, open(output_file_path, 'w', encoding='utf-8') as output_file:
        input_content = input_file.read()

        # Replace HTML entities with their character forms
        input_content = html.unescape(input_content)
        
        # Find all matches of the token pattern in the input content
        token_matches = re.findall(token_pattern, input_content)
        
        # Write lemma, tag, and text for each token into the output file
        for lemma, tag, text in token_matches:
            output_line = f"{text} {lemma} {tag}\n"
            output_file.write(output_line)
    
    print("Extraction and writing completed.")

    
if __name__ == "__main__":
    input_text = "src/models_comparison/BiLSTM_results/input_named_ent_etest_raw.txt"
    output_xml_file = "src/models_comparison/BiLSTM_results/output.xml"
    convert_to_xml(input_text, output_xml_file)
    
    input_xml_file = output_xml_file
    output_txt_file = "src/models_comparison/BiLSTM_results/input_named_ent_etest_clean.conll"
    extract_lemmas_tags_and_text(input_xml_file, output_txt_file)
    
    sys.path.remove(module_dir)
    print("Tags and Lemmas extracted from input_named_ent_etest_raw.txt and saved to input_named_ent_etest_clean.conll")
    
    try:
        os.remove(output_xml_file)
        print(f"File '{output_xml_file}' has been removed.")
    except OSError as e:
        print(f"Error while removing '{output_xml_file}': {e}")