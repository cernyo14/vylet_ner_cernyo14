import regex
import re

def clean(inFilePath: str, outFilePath: str):
    '''
    Remove lines that contain only special characters
    '''
    in_file_lines = open(inFilePath, 'r', encoding='utf-8').readlines()
    
    with open(outFilePath, 'w', encoding='utf-8') as outFile:
        for line in in_file_lines:
            token = re.split(r'\s+|\t', line.strip())[0]
            if regex.search(r'[\p{L}\p{N}]', token):
                outFile.write(line)
                
def clear(editFilePath: str, trueFilePath: str):
    '''
    Our file tagged by NameTag has still some extra words so we will clear these extra words
    Keep only words if they match in true and also in the pred file
    '''
    true_lines = open(trueFilePath, 'r', encoding='utf-8').readlines()
    edit_lines = open(editFilePath, 'r', encoding='utf-8').readlines()
    
    
    with open(editFilePath, "w", encoding="utf-8") as edit_file:
        
        index_true = 0
        for index_edit, edit_line in enumerate(edit_lines):
            edit_token = re.split(r'\s+|\t', edit_line.strip())[0]
            
            if index_edit + 1 != len(edit_lines):
                edit_token_next = re.split(r'\s+|\t', edit_lines[index_edit + 1].strip())[0]
                
            true_token = re.split(r'\s+|\t', true_lines[index_true].strip())[1]
            
            if index_true + 1 != len(true_lines):
                true_token_next = re.split(r'\s+|\t', true_lines[index_true + 1].strip())[1]
                true_cat_next = re.split(r'\s+|\t', true_lines[index_true + 1].strip())[-1]

            if edit_token != true_token and edit_token_next == true_token:
                continue 
            
            if edit_token != true_token and edit_token == true_token_next:
                edit_file.write(f"{true_token}\t{true_cat_next}\n")
                index_true += 1
                
            index_true += 1
            edit_file.write(edit_line)
                
if __name__ == "__main__":
    clean("src/models_comparison/NameTag_results/output_ccv_test_BIO.txt", "src/models_comparison/NameTag_results/output_ccv_test_BIO_cleaned.txt")
    clear("src/models_comparison/NameTag_results/output_ccv_test_BIO_cleaned.txt", "src/models_comparison/files_to_compare/ccv_test_correct_cleaned.txt")