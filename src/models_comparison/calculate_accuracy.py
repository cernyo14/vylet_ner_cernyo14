import re

def calculate_accuracy(true_file_path, predicted_file_path, calculate_mystic = False):
    '''
    Calculate accuracy of predicated model (predicted_file_path) compared to true values (true_file_path)
    '''
    true_lines = open(true_file_path, 'r', encoding='utf-8').readlines()
    predicted_lines = open(predicted_file_path, 'r', encoding='utf-8').readlines()
    
    if len(true_lines) != len(predicted_lines):
        raise ValueError("Files have different lengths")
    
    correct_predictions = 0
    total_predictions = 0
    
    for true_line, predicted_line in zip(true_lines, predicted_lines):
        # skip lines with empty space only (these lines just separate stanzas)
        if not true_line.strip() and not predicted_line.strip():
            continue
        
        true_label = re.split(r'\s+|\t', true_line.strip())[-1]
        predicted_label = re.split(r'\s+|\t', predicted_line.strip())[-1]

        # since B-MYSTIC PERSON will be split by the prev code to just PERSON but normal person will be shown as B-PERSON
        if (not calculate_mystic) and true_label[2:] == "MYSTIC":
            continue
        
        if true_label == predicted_label:
            correct_predictions += 1
        
        total_predictions += 1
    
    accuracy = correct_predictions / total_predictions
    return accuracy

def calculate_accuracy_only_B_entities(true_file_path, predicted_file_path, calculate_mystic = False):
    '''
    Calculate accuracy of predicated model (predicted_file_path) compared to true values (true_file_path)
    Count every entity only ones so when a words has both the predicted and the true label as I-EntityType it will not be counted
    '''
    true_lines = open(true_file_path, 'r', encoding='utf-8').readlines()
    predicted_lines = open(predicted_file_path, 'r', encoding='utf-8').readlines()
    

    if len(true_lines) != len(predicted_lines):
        raise ValueError("Files have different lengths")
    
    correct_predictions = 0
    total_predictions = 0
    
    for true_line, predicted_line in zip(true_lines, predicted_lines):
        # skip lines with empty space only (these lines just separate stanzas)
        if not true_line.strip() and not predicted_line.strip():
            continue
        
        true_label = re.split(r'\s+|\t', true_line.strip())[-1]
        predicted_label = re.split(r'\s+|\t', predicted_line.strip())[-1]
                    
        # since B-MYSTIC PERSON will be split by the prev code to just PERSON but normal person will be shown as B-PERSON
        if (not calculate_mystic) and true_label[2:] == "MYSTIC":
            continue
        
        if true_label[0] == "I" and predicted_label[0] == "I":
            continue
        
        if true_label == predicted_label:
            correct_predictions += 1
        
        total_predictions += 1
    
    accuracy = correct_predictions / total_predictions
    return accuracy

def calculate_accuracy_only_true_entities(true_file_path: str, pred_file_path: str, calculate_mystic = False) -> float:
    '''
    Calculate accuracy of predicated model (pred_file_path) compared to true values (true_file_path)
    Count every entity only ones so only words that are marked as entity in the true file
    '''
    true_lines = open(true_file_path, 'r', encoding='utf-8').readlines()
    pred_lines = open(pred_file_path, 'r', encoding='utf-8').readlines()
    

    if len(true_lines) != len(pred_lines):
        raise ValueError("Files have different lengths")
    
    correct_predictions = 0
    total_predictions = 0
    
    for true_line, pred_line in zip(true_lines, pred_lines):
        # skip lines with empty space only (these lines just separate stanzas)
        if not true_line.strip() and not pred_line.strip():
            continue
        
        true_label = re.split(r'\s+|\t', true_line.strip())[-1]
        pred_label = re.split(r'\s+|\t', pred_line.strip())[-1]
        
        # since B-MYSTIC PERSON will be split by the prev code to just PERSON but normal person will be shown as B-PERSON
        if (not calculate_mystic) and true_label[2:] == "MYSTIC":
            continue
        
        if true_label[0] != "B" or pred_label[0] != "B":
            continue
        
        if true_label == pred_label:
            correct_predictions += 1
        
        total_predictions += 1
    
    return correct_predictions / total_predictions

def calculate_accuracy_no_category(true_file_path: str, pred_file_path: str, calculate_mystic = False) -> float:
    '''
    Calculate accuracy of predicated model (pred_file_path) compared to true values (true_file_path)
    Calculate accuracy regardless of entity type/category
    Calculating only if word is correctly labeled as an entity or not (regardless of type/category)
    '''
    true_lines = open(true_file_path, 'r', encoding='utf-8').readlines()
    pred_lines = open(pred_file_path, 'r', encoding='utf-8').readlines()
    
    if len(true_lines) != len(pred_lines):
            raise ValueError("Files have different lengths")
    
    correct_predictions = 0
    total_predictions = 0
    
    for true_line, pred_line in zip(true_lines, pred_lines):
        # skip lines with empty space only (these lines just separate stanzas)
        if not true_line.strip() and not pred_line.strip():
            continue
        
        true_label = re.split(r'\s+|\t', true_line.strip())[-1]
        pred_label = re.split(r'\s+|\t', pred_line.strip())[-1]
            
        # since B-MYSTIC PERSON will be split by the prev code to just PERSON but normal person will be shown as B-PERSON
        if (not calculate_mystic) and true_label[2:] == "MYSTIC":
            continue
        
        if not (true_label[0] == "I" or true_label[0] == "B"):
            continue
        
        if pred_label[0] == "I" or pred_label[0] == "B":
            correct_predictions += 1
        
        total_predictions += 1
        
    return correct_predictions / total_predictions

'''
true_output_file = "model_implementation/inputs/Stanzas_newer_version_test.conll"
BiLSTM_output_file = "model_implementation/outputs/Stanzas_newer_version_test.txt"
NameTag_output_file = "nametag-2.0.0/outputs/ccv_stanzas_newer_version_test.txt"

accuracy = calculate_accuracy(true_output_file, BiLSTM_output_file)
print(f"Accuracy: {accuracy:.2f}%")
accuracy_with_B = calculate_accuracy_only_B_entities(true_output_file, NameTag_output_file)
print(f"Accuracy with B-entities only: {accuracy_with_B:.2f}%")
'''