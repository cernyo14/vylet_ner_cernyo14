import re
import html

def remove_xml_tags_from_file(input_file, output_file):
    with open(input_file, 'r', encoding='utf-8') as file:
        xml_text = file.read()

    # Use regular expressions to remove XML tags
    clean_text = re.sub(r'<.*?>', '', xml_text)
    
    # Convert special XML characters back to their originals
    clean_text = html.unescape(clean_text)

    # Write the cleaned text to a text file
    with open(output_file, 'w', encoding='utf-8') as file:
        file.write(clean_text)

if __name__ == "__main__":
    input_file = "./Czech_Named_Entity_Corpus_2.0/cnec2.0/data/xml/named_ent_etest.xml"
    output_file = "./src/models_comparison/files_to_compare/named_ent_etest.txt"
    remove_xml_tags_from_file(input_file, output_file)
    print("XML tags removed and text saved to output.txt")