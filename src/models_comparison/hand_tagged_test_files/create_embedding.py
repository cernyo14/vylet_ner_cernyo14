import json
from gensim.models import Word2Vec
import gzip
import os
import sys

script_dir = os.path.dirname(os.path.abspath(__file__))
module_dir = os.path.join(script_dir, '../..')
sys.path.append(module_dir)
import constants

def save_embedding(model, vocab: list, path: str):
    tmp_path = "tmp.txt"
    
    with open(tmp_path, "w", encoding='utf-8') as f:
        for word in vocab:
            f.write(word + " " + " ".join([str(x) for x in model.wv[word]]) + "\n")
            
    with open(tmp_path, "rb") as f:
        with gzip.open(path, "wb") as f_gz:
            f_gz.writelines(f)
            
    os.remove(tmp_path)

def create_embedding(lemma_json_file: str):
    with open(lemma_json_file, 'r', encoding="utf-8") as json_file:
        lemma_data_json = json.load(json_file)
        
    json_data = lemma_data_json["result"]
    sentences = []
    
    for sentence in json_data:
        sentences.append([word["lemma"] for word in sentence])    
    
    model_lemma = Word2Vec(sentences=sentences, vector_size=100, window=5, min_count=1, workers=8)
    
    vocab = [word for word in model_lemma.wv.index_to_key]
    
    save_embedding(model_lemma, vocab, "src/models_comparison/hand_tagged_test_files/cs_word_2_vec_lemma.txt.gz")
    
    #word = "být"
    #if model_lemma.wv.get_vecattr(word, "count"):
    #    embedding = model_lemma.wv[word]
    #    print(f"Embedding for '{word}':\n{embedding}")
    #else:
    #    print(f"'{word}' is not in the vocabulary.")
    
def create_lemma_input(lemma_json_file: str, cs_true: str, output_file_true: str, output_file_input: str):
    with open(lemma_json_file, 'r', encoding="utf-8") as json_file:
        json_data= json.load(json_file)
        
    with open(cs_true, 'r', encoding="utf-8") as f:
        token_tags_lines = f.readlines()
    
    lemma_list = [word for sentence in json_data["result"] for word in sentence if word["token"] not in constants.LINGUISTIC_MARKS]
    
    res_true = ""
    res_input = ""
    idx = 0
    for line in token_tags_lines:
        if line == "\n":
            res_true += "\n"
            res_input += "\n"
            continue
        
        line_tag = line.split("\t")[-1]
        token = line.split("\t")[0]
        
        res_true += f"{lemma_list[idx]['lemma']}\t{line_tag}"
        res_input += f"{lemma_list[idx]['lemma']}\n"
        idx += 1
        
        if token[len(token) - 2:] == "’s":
            idx += 1
            
    
    with open(output_file_true, 'w', encoding="utf-8") as f:
        f.write(res_true)
        
    with open(output_file_input, 'w', encoding="utf-8") as f:
        f.write(res_input)
    
    
    
create_lemma_input("src/models_comparison/hand_tagged_test_files/cs_lemmatized.txt", "src/models_comparison/hand_tagged_test_files/cs_true.txt", "src/models_comparison/hand_tagged_test_files/cs_true_lemma.txt", "src/models_comparison/hand_tagged_test_files/cs_input_lemma.conll")       
#create_embedding("src/models_comparison/hand_tagged_test_files/cs_lemmatized.txt")