import json
import os
import sys

script_dir = os.path.dirname(os.path.abspath(__file__))
module_dir = os.path.join(script_dir, '../..')
sys.path.append(module_dir)
import constants

def convert_entity_tag(tag: str) -> str:
    """
    Convert entity tags in json files to tags we use
    """
    if tag == "LOC":
        return constants.PLACE
    if tag == "PER":
        return constants.PERSON
    
    # this last return is here just as a safety (the json should not include other tags then LOC or PER)
    return constants.UNDEFINED


def get_entity(word_index: int, poem_dict: dict) -> str:
    """
    Based on the word index return entity tag for given word (if word with this index has an entity tag)
    """
    for ids_index, ids in enumerate(poem_dict["ids"]):
        for id_index, id in enumerate(ids):
            if id != word_index:
                continue
            
            tag = convert_entity_tag(poem_dict["tags"][ids_index])
            if id_index == 0:
                return f'B-{tag}'
            return f'I-{tag}'
        
    return f"{constants.UNDEFINED}"

def get_poem_txt(poem_id: str, poem_dict: dict, poems_data: dict, entity_col: bool, plain_txt: bool) -> str:
    '''
    Converts a poem into a string of tokens
    '''
    res = ""
    curr_line_idx = 0
    curr_sentence_idx = 0
    
    for word_index, word in enumerate(poems_data["samples"][poem_id]):
        
        if word["token"] in constants.LINGUISTIC_MARKS and not plain_txt:
            continue
        
        if word["line"] > curr_line_idx:
            curr_line_idx = word["line"]
            res += "\n\n"
            
        if word["sentence"] > curr_sentence_idx:
            curr_sentence_idx = word["sentence"]
            res += "\n"
        
        if plain_txt:
            res += f'{word["token"]} '
            continue
            
        if not entity_col:
            res += f'{word["token"]}\n'
            continue
            
        word_tag = get_entity(word_index, poem_dict)
        res += f'{word["token"]}\t{word_tag}\n'
    
    res += "\n"    
    return res

def convert_to_txt_file(ents_file: str, peoms_file: str, output_file: str, entity_col = True, plain_txt = False):
    with open(ents_file, 'r', encoding="utf-8") as json_file:
        ents_data = json.load(json_file)
        
    with open(peoms_file, 'r', encoding="utf-8") as json_file:
        poems_data = json.load(json_file)
    
    all_txt = ""
    for poem_id, poem_dict in ents_data["gold"].items():
        all_txt += get_poem_txt(poem_id, poem_dict, poems_data, entity_col, plain_txt)
    
    with open(output_file, 'w', encoding="utf-8") as out_file:
        out_file.write(all_txt)
        
    
#convert_to_txt_file("src/models_comparison/hand_tagged_test_files/cs.json", "src/models_comparison/hand_tagged_test_files/cs_text.json", "src/models_comparison/hand_tagged_test_files/cs_true.txt")
#convert_to_txt_file("src/models_comparison/hand_tagged_test_files/cs.json", "src/models_comparison/hand_tagged_test_files/cs_text.json", "src/models_comparison/hand_tagged_test_files/cs_input.conll", False)
convert_to_txt_file("src/models_comparison/hand_tagged_test_files/cs.json", "src/models_comparison/hand_tagged_test_files/cs_text.json", "src/models_comparison/hand_tagged_test_files/cs_plain.txt", False, True)