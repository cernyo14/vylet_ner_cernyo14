import json
import os
import sys
import re

script_dir = os.path.dirname(os.path.abspath(__file__))
module_dir = os.path.join(script_dir, '../..')
sys.path.append(module_dir)
import constants

id_counter = 1

def word_to_dict(word: dict) -> dict:
    res_word = {}   
    res_word["token"] = word["token"]
    res_word["lemma"] = re.split(r'[-_`]', word["lemma"])[0]
    res_word["morph"] = word["tag"]
    
    global id_counter
    res_word["id"] = id_counter
    id_counter += 1
    
    return res_word

def get_line_text(word_token_list: list) -> str:
    res = ""
    for word in word_token_list:
        if word in constants.LINGUISTIC_MARKS or word == word_token_list[0]:
            res += word
        else:
            res += f" {word}"
            
    return res

def convert_to_book_json(txt_file: str, morph_json: json, book_json: json):        
    with open(morph_json, 'r', encoding="utf-8") as json_file:
        morph_data = json.load(json_file)["result"]
        
    book = []
    poem = {}
    poem_body = []
    stanza = []
    line = {}
    
    
    for line_morph in morph_data:
        line_idx = 0
        line_words = []
        line_punct = {}
        for word in line_morph:
            if word["token"] in constants.LINGUISTIC_MARKS:
                if str(line_idx) in line_punct:
                    line_punct[str(line_idx)] += word["token"]
                else:
                    line_punct[str(line_idx)] = word["token"]
                continue
            
            line_words.append(word_to_dict(word))
            line_idx += 1
        
        line["text"] = get_line_text([word["token"] for word in line_morph])
        line["punct"] = line_punct
        line["words"] = line_words        
        stanza.append(line)
        line = {}
        
        if word["space"] == " \r\n\r\n\r\n":
            poem_body.append(stanza)
            stanza = []
    
    poem["body"] = poem_body
    book.append(poem)
    
    with open(book_json, "w") as json_file:
        json.dump(book, json_file, indent=4)
        


txt_file = "src/models_comparison/hand_tagged_test_files/cs_plain.txt"
morph_json = "src/models_comparison/hand_tagged_test_files/cs_morph_tagged_raw.json"
res_file = "src/models_comparison/hand_tagged_test_files/cs_book.json"

convert_to_book_json(txt_file, morph_json, res_file)