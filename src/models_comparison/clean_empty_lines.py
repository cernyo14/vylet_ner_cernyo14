def clean(in_file_path: str, out_file_path: str):
    in_file_lines = open(in_file_path, 'r', encoding='utf-8').readlines()
    
    with open(out_file_path, 'w', encoding='utf-8') as outFile:
        for line in in_file_lines:
            if line.strip():
                outFile.write(line)
                
if __name__ == "__main__":
    clean("src/models_comparison/BiLSTM_results/ccv_test_output.txt", "src/models_comparison/BiLSTM_results/ccv_test_output_cleaned.txt")
    clean("src/models_comparison/files_to_compare/ccv_test_correct.txt", "src/models_comparison/files_to_compare/ccv_test_correct_cleaned.txt")