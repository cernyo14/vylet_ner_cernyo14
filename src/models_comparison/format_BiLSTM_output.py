from NameTag_to_BIO import split_words

def format_output(input_file, output_file):
    '''
    Formats output from the BiLSTM model so it can be comparable with the other NameTag model
    '''
    
    with open(input_file, 'r', encoding='utf-8') as infile:
        input_text = infile.readlines()
    
    with open(output_file, 'w', encoding='utf-8') as outfile:    
        
        for line in input_text:
            line_arr = line.split()
            if len(line_arr) != 3:
                continue
            label = line_arr[2]
            # change MYSTIC to PERSON
            if label == "B-MYSTIC":
                label = "B-PERSON"
            elif label == "I-MYSTIC":
                label = "I-PERSON"
            elif label == "B-OTHER":
                label = "O"
            elif label == "I-OTHER":
                label = "O"
                
            words = split_words(line_arr[0])
            for word in words:
                outfile.write(word + "\t" + label + "\n")
    

if __name__ == "__main__":
    input_file = "src/models_comparison/BiLSTM_results/output_named_ent_etest.txt"
    output_file = "src/models_comparison/BiLSTM_results/clean_BIO_output_named_ent_etest.txt"
    format_output(input_file, output_file)
    print("Done!")