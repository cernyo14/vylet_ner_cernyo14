from bs4 import BeautifulSoup
from NameTag_to_BIO import get_bio_type, split_words

def get_elem_words(elem) -> list:
    if not hasattr(elem, 'tag'):
        return split_words(elem)
    
    words = []
    for child in elem.contents:
        words.extend(split_words(child.text))
    
    return words
    
def get_words_tag(elem, output_file):
    if not hasattr(elem, 'tag'):
        for word in split_words(elem):
            output_file.write(f"{word}\tO\n")
        return
    
    ne_type = get_bio_type(elem.attrs['type'], True)
    if not ne_type == "O":
        first = True
        for word in get_elem_words(elem):
            output_file.write(f"{word}\t{get_bio_type(elem.attrs['type'], first)}\n")
            first = False
        return
    
    for child in elem.contents:
        get_words_tag(child, output_file)

def cnec_to_bio(input_xml: str, output_txt: str):
    with open(input_xml, 'r', encoding='utf-8') as input_file:
        xml_content = input_file.read()

    soup = BeautifulSoup(xml_content, 'lxml')

    with open(output_txt, 'w', encoding='utf-8') as output_file:
        for elem in soup.findAll()[2].contents:
            if hasattr(elem, 'tag'):                
                get_words_tag(elem, output_file)

            if not isinstance(elem, str):
                continue
            words = split_words(elem)
            for word in words:
                output_file.write(f"{word}\tO\n")

if __name__ == "__main__":
    input_file_path = 'Czech_Named_Entity_Corpus_2.0/cnec2.0/data/xml/named_ent_etest.xml'
    #input_file_path = 'src/models_comparison/files_to_compare/text.xml'

    output_file_path = 'src/models_comparison/files_to_compare/named_ent_etest_BIO_correct.txt'
    cnec_to_bio(input_file_path, output_file_path)
