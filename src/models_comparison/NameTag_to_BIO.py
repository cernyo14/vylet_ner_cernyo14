import xml.etree.ElementTree as ET
import sys
import os

script_dir = os.path.dirname(os.path.abspath(__file__))
module_dir = os.path.join(script_dir, '..')
sys.path.append(module_dir)
import constants

def split_words(input_txt: str) -> list:
    '''
    Splits input string into separate words
    Also splits all linguistic marks as separate words
    by using constant LINGUISTIC_MARKS
    :param input_txt: string to be split
    :return: list of words
    '''
    words = []
    word = ''
    prev_number = False
    for char in input_txt:
        if char.isspace():
            if word:
                words.append(word)
                word = ''
            continue
                
        if char in constants.NUMBERS:
            if word and not prev_number:
                words.append(word)
                word = ''
            prev_number = True
            word += char
            continue
        
        if prev_number:
            if word:
                words.append(word)
                word = ''
            prev_number = False   

        if char in constants.LINGUISTIC_MARKS:
            if word:
                words.append(word)
                word = ''
            words.append(char)
            continue
        
        if char.isupper():
            if word and not word[-1].isupper():
                words.append(word)
                word = ''
        
        word += char

    
    if word:
        words.append(word)
        
    return words

def get_bio_type(bilou_tag: str, first: bool) -> str:
    '''
    Converts BILOU tag to BIO tag
    @param bilou_tag: BILOU tag
    @param first: True if the first token of the entity, False otherwise
    @return: BIO tag
    '''
    if bilou_tag.lower()[0] != 'p' and bilou_tag.lower()[0] != 'g':
        return "O"
    
    if first:
        res = 'B-'
    else:
        res = 'I-'
    
    if bilou_tag.lower()[0] == 'p':
        return res+"PERSON"
    
    return res+"PLACE"

def get_elem_words(elem) -> list:
    if elem.tag == 'token':
        return split_words(elem.text)
    
    words = []
    for token in elem.iter('token'):
        words.extend(split_words(token.text))
    
    return words

def get_words_tag(elem, output_file):
    if elem.tag == 'token':
        for word in split_words(elem.text):
            output_file.write(f"{word}\tO\n")
        return
    
    ne_type = get_bio_type(str(elem.get('type')), True)
    if not ne_type == "O":
        first = True
        for word in get_elem_words(elem):
            output_file.write(f"{word}\t{get_bio_type(str(elem.get('type')), first)}\n")
            first = False
        return
    
    for child in elem:
        get_words_tag(child, output_file)
    
def bilou_to_bio(input_file, output_file):
    '''
    Converts BILOU tagging to BIO tagging
    @param input_file: input file with BILOU tagging created by NameTag model
    @param output_file: output file with BIO tagging   
    '''
    tree = ET.parse(input_file)
    root = tree.getroot()
    
    with open(output_file, 'w', encoding='utf-8') as txt_file:
        for sentence in root.findall('sentence'):
            for child in sentence:
                
                first = True
                if child.tag == 'ne':
                    get_words_tag(child, txt_file)

                if child.tag == 'token':   
                    for word in split_words(child.text):      
                        txt_file.write(f"{word}\tO\n")


def convert_to_xml(input_file, output_file):
    '''
    Converts input text file (XML like file) to XML file
    @param input_text: input text file
    @param output_file: output XML file 
    '''
    with open(input_file, 'r', encoding='utf-8') as file:
        input_text = file.read()

    sentences = input_text.strip().split('</sentence>')

    with open(output_file, 'w', encoding='utf-8') as xml_file:
        xml_file.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
        xml_file.write("<root>\n")
        for sentence in sentences:
            sentence = sentence.strip()
            if sentence:
                xml_file.write(sentence + "</sentence>\n")
        xml_file.write("</root>\n")
        

if __name__ == "__main__":
    input_text = "./src/models_comparison/NameTag_results/output_ccv_test.txt"
    output_xml_file = "./src/models_comparison/output.xml"
    convert_to_xml(input_text, output_xml_file)
    
    input_xml_file = output_xml_file
    output_txt_file = "./src/models_comparison/NameTag_results/output_ccv_test_BIO.txt"
    bilou_to_bio(input_xml_file, output_txt_file)
    
    sys.path.remove(module_dir)
    print("XML converted to BIO and saved to output_ccv_test_BIO.txt")
    
    try:
        os.remove(output_xml_file)
        print(f"File '{output_xml_file}' has been removed.")
    except OSError as e:
        print(f"Error while removing '{output_xml_file}': {e}")
        
    