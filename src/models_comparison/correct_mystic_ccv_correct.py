import re

def correct(file_path: str):
    '''
    In the correct ccv files Mystic Person entities are labeled as B/I-MYSTIC PERSON
    The space between MYSTIC and PERSON causes a bit of a mayhem so we decided to labeled them simply as MYSTIC
    Since that is what our BiLSTM model does by default
    '''
    file_lines = open(file_path, 'r', encoding='utf-8').readlines()
    
    with open(file_path, 'w', encoding='utf-8') as outFile:
        for line in file_lines:
            split_line = re.split(r'\s+|\t', line.strip())
            if split_line[-1] == "PERSON":
                split_line = split_line[:-1]
                
                # Remove Real Person entities as well
                if split_line[-1][2:] == "REAL":
                    last_elem = list(split_line[-1])
                    last_elem[2:] = "PERSON"
                    split_line[-1] = "".join(last_elem)
                    
            outFile.write("\t".join(split_line) + "\n")       
            
if __name__ == "__main__":
    true_output_file = "model_implementation/inputs/Stanzas_newer_version_train.conll"
    BiLSTM_output_file = "model_implementation/outputs/Stanzas_newer_version_test.txt"
    NameTag_output_file = "nametag-2.0.0/outputs/ccv_stanzas_newer_version_test.txt"
    
    correct(true_output_file)
    correct(BiLSTM_output_file)
    correct(NameTag_output_file)