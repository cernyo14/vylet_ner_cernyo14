import requests
import wikipedia
import constants
import multiprocessing
import random
import urllib.request
from urllib.request import urlopen
from book_word import Word

from bs4 import BeautifulSoup as bs
from entity import Entity
import constants
import pickle
import time

import os


def init_worker(val):
    global counter
    counter = val
    
def is_real_place(soup) -> bool:
    """
    Check if the entity is a real place.
    """
    if soup.find("div", {"id": "mw-indicator-coordinates"}):
        return True
    else:
        return False
    
def is_real_person(soup) -> bool:
    """
    Check if the entity is a real person.
    """    
    for item in soup.find_all('table', {"class": "infobox"}):
        if item.find(string="Narození"):
            return True
        if item.find(string="Datum narození"):
            return True
        
    return False
    
def contains_warning(soup) -> bool:
    """
    Check if page contains warning at the top of the page.
    """    
    if soup.find('div', {"class": "uvodni-upozorneni hatnote noprint"}):
        return True
        
    return False

def get_type_existing_page(page_py: wikipedia.WikipediaPage, ent_string: str) -> str:
    '''
    Get entity type from existing wiki page.
    '''
    req = requests.get(page_py.url).content
    soup = bs(req, 'html.parser')

    if is_real_person(soup):
        return constants.PERSON

    elif is_real_place(soup):
        if soup.find("div", {"class": "uvodni-upozorneni"}):
            return constants.DISAMBIGUATION
        return constants.PLACE
    else:
        for category in page_py.categories:                    
            if category.find("příjmení") != -1: 
                return constants.PERSON
            
            if category.find("jména") != -1: 
                return constants.PERSON
            
            if category.lower().find("národy") != -1:
                return constants.PERSON
            
            if category.lower().find("mytologi") != -1:
                return constants.MYSTIC_PERSON
            
            if category.lower().find("postav") != -1:
                return constants.PERSON
            
            if category.lower().find("bohové") != -1:
                return constants.MYSTIC_PERSON
            
            if category.lower().find("bohyně") != -1:
                return constants.MYSTIC_PERSON
            
            if category.lower().find("bůh") != -1:
                return constants.MYSTIC_PERSON
            
            if category.lower().find("král") != -1:
                return constants.PERSON
            
            if category.lower().find("panov") != -1:
                return constants.PERSON
            
            if category.find("místopis") != -1: 
                return constants.PLACE
            
            if category.lower().find("míst") != -1:
                return constants.PLACE
            
            if category.lower().find("stát") != -1:
                return constants.PLACE
            
            if category.lower().find("země") != -1:
                return constants.PLACE
            
            if category.lower().find("území") != -1:
                return constants.PLACE
            
            if category.lower().find("geograf") != -1:
                return constants.PLACE
            
            if category.lower().find("vojvodství") != -1:
                return constants.PLACE
            
            if category.lower().find("kraj") != -1:
                return constants.PLACE
            
            if category.lower().find("říše") != -1:
                return constants.PLACE
            
            if category.lower().find("územ") != -1:
                return constants.PLACE
            
            if category.lower().find("ostrov") != -1:
                return constants.PLACE
            
            if category.lower().find("okres") != -1:
                return constants.PLACE
            
            if category.lower().find("středisk") != -1:
                return constants.PLACE
            
            if category.lower().find("rozcest") != -1:
                return constants.DISAMBIGUATION
        
        
        url_ent_string = urllib.parse.quote(ent_string)
        url = f'https://www.wikidata.org/w/index.php?go=Go&search={url_ent_string}&search={url_ent_string}&title=Special%3ASearch&ns0=1&ns120=1'
        req = requests.get(url).text
        soup = bs(req, 'lxml')
        
        return constants.NOT_EXISTS

def get_ent_type_str(ent_string: str) -> str:
    '''
    Get entity type from wiki page using entity lemma as a search word in wiki.
    '''
    wikipedia.set_lang("cs")

    try:
        page_py = wikipedia.WikipediaPage(ent_string) #or .page which return first result in search
        return get_type_existing_page(page_py, ent_string)
    
    except wikipedia.exceptions.DisambiguationError as e:
        return constants.DISAMBIGUATION
    
    except wikipedia.exceptions.PageError:
        return constants.NOT_EXISTS
        
    except:
        return constants.NOT_EXISTS
    

def check_type_single_ent(ent: Entity) -> Entity:
    """
    Single process function for checking the type of the entity.
    return: entity with its type
    """
    ent_string = ent.to_str_lemma_capital()
    ent.set_type(get_ent_type_str(ent_string))
    
    global counter
    with counter.get_lock():
        counter.value += 1
    if counter.value % 100 == 0:
        print(counter.value)
        
    return ent


def main(entities: list, size = 0) -> list:
    """
    Checks the category/type of all unique entities.
    param entities: list of all unique entities across all books
    param size: number of entities to check, if 0, all entities are checked
    return: list of entities with their categories/types
    """  
        
    ents = list()
    if size == 0 or size > len(entities):
        size = len(entities)
        ents = entities
    else:
        for _ in range(size):
            ents.append(entities[random.randint(0, len(entities) - 1)])
            
    val = multiprocessing.Value('i', 0)
    
    print("Starting multiprocessing")
    print("Number of processes: ", multiprocessing.cpu_count())
    print("Number of entities categorized (prints every 100th):")
    
    with multiprocessing.Pool(multiprocessing.cpu_count(), initializer=init_worker, initargs=(val, )) as pool:
        results = pool.map(check_type_single_ent, ents)
        
    return results



if __name__ == '__main__':
    #novy Zamek vyzkouset, Erechteion, Synbozi, duch svaty, Jezisek, Chesterton, Byzance
    #print(check_type_single_ent(Entity([Word("Petřín", "----", "Petřín", "", 5)])).category)
    #print(get_ent_type_str("Duch Svatý"))
        
    entities = []
    
    with open('./src/pickles/cs_potential_ents_uniq', 'rb') as f:
        entities = pickle.load(f)
    
    print("Total number of entities:", len(entities))    
    start = time.time()
    res = main(entities)
    end = time.time()

    file_ents = "./src/pickles/cs_ents_types_all_wiki_only_no_other_less_all.plk"

    with open(file_ents, "wb") as f:
        pickle.dump(res, f)
        
    with open('cs_output_ents_types_all_wiki_only_no_other_less_all.txt', 'w', encoding='utf-8') as file:
        for ent in res:
            print(f"{ent} / {ent.to_str_lemma()} / {ent.category}", file=file)
    
    print("All done.")
